<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    //
    protected $fillable = [
        'user_id',
        'data',
        'title',
        'type',
        'likes',
        'loves',
        'created_at',
    ];
    protected $table = 'socials';
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }



    public function is_like (){


    }
}

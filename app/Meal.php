<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    //
    protected $fillable = [
        'name',
    ];
    protected $table = 'meals';

    public function products(){
        return $this->hasMany('App\Product','meal_id','id');
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\Http\Resources\CityResource;
use App\Http\Resources\MealResource;
use App\Http\Resources\PackageResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\RegionResource;
use App\Http\Resources\SocialResource;
use App\Http\Resources\SocialResource2;
use App\Http\Resources\SportResource;
use App\Http\Traits\ApiResponses;
use App\Meal;
use App\Package;
use App\Product;
use App\Regions;
use App\Social;
use App\Sport;
use JWTAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    //
    use ApiResponses;

    public function getSports(){

        $sports = Sport::all();

        $sports = new SportResource($sports);
        return $this->apiResponse($sports);

    }

    public function getCities(){
        $cities = City::all();

        $cities = new CityResource($cities);
        return $this->apiResponse($cities);
    }

    public function getRegionByCityId($city_id){
        $regions = Regions::where('city_id',$city_id)->get();

        $regions = new RegionResource($regions);
        return $this->apiResponse($regions);
    }

    public function getSocials(){

        $socials = Social::all();
        $socials = new SocialResource($socials);
        return $this->apiResponse($socials);

    }

    public function getSocialsNotAuth(){

        $socials = Social::all();
        $socials = new SocialResource2($socials);
        return $this->apiResponse($socials);

    }

    public function getMeals(){

        $meals = Meal::all();

        $meals = new MealResource($meals);
        return $this->apiResponse($meals);

    }

    public function getPackages(){

        $packages = Package::where('price','>','0')->get();

        $packages = new PackageResource($packages);
        return $this->apiResponse($packages);

    }

    public function getProductsWithMeal($meal_id){

        $products = Product::where('meal_id',$meal_id)->get();
        $products = new ProductResource($products);
        return $this->apiResponse($products);

        
    }


    public function getAllProducts(){

        $products = Product::orderBy('created_at','DESC')->get();
        $products = new ProductResource($products);
        return $this->apiResponse($products);


    }

}

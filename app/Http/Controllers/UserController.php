<?php

namespace App\Http\Controllers;

use App\Cart;
use App\CartProduct;
use App\Chat;
use App\Http\Resources\CartProductResource;
use App\Http\Resources\CartResource;
use App\Http\Resources\ChatMessageResource;
use App\Http\Resources\ChatResource;
use App\Http\Resources\PackageUserResource;
use App\Http\Resources\SocialResource;
use App\Http\Resources\SportUserResource;
use App\Http\Resources\UserChatResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\WatersResource;
use App\Http\Traits\ApiResponses;
use App\Like;
use App\Package;
use App\PackageUser;
use App\PayNumber;
use App\Product;
use App\Social;
use App\Sport;
use App\SportUser;
use App\User;
use App\Water;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Namshi\JOSE\JWT;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    use ApiResponses;
    
    public function authenticate(Request $request)
    {
        $credentials = $request->only('phone', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials,['exp' => Carbon::now()->addDays(30)->timestamp])) {
                return response()->json(
                    [
                        'status' => false,
                        'message' => 'من فضلك ادخل البيانات صحيحة '
                    ]
                    , 400);
            }
        } catch (JWTException $e) {
            return response()->json([
                    'status' => false,
                    'message' => 'من فضلك ادخل البيانات صحيحة '
                ]
                , 400);
        }


        $userData = User::where('phone',$request->get('phone'))->first();

        $checkUserHasPackage = PackageUser::where('user_id',$userData->id)
            ->where('status','1')
            ->first();

        $check = false;
        if($checkUserHasPackage){
            // check is free package or not
            if($checkUserHasPackage->package->price == 0){
                $check = false;
            }elseif ($checkUserHasPackage->number_of_meals >= $checkUserHasPackage->current_number_of_meals){

                $check = true;

            }
        }

        $status = true;
        $user = [
                    'id'            =>$userData->id,
                    'name'          =>$userData->name,
                    'phone'         =>$userData->phone,
                    'image'         =>getImg($userData->image),
                    'email'         =>$userData->email,
                    'address'         =>$userData->address,
                    'weight'         =>$userData->weight,
                    'tall'         =>$userData->height,
                    'age'         =>$userData->age,
                    'type'         =>$userData->type,
                    'fitness'         =>$userData->fitness,
                    'default_water'         =>$userData->default_water,
                    'active'        =>$userData->active,
                    'has_package'        =>$check,
                    'active_message' =>$userData->active_message,
                    'city_id' =>$userData->city->id,
                    'city_name' =>$userData->city->name,
                    'region_id' =>$userData->region->id,
                    'region_name' =>$userData->region->name,
                    'token' =>$token,
            ];
        $message = "بيانات المستخدم ";
        return response()->json(compact('status','message','user'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'region_id' => 'required',
            'city_id' => 'required',
            'phone' => 'required|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ],[
                'name.required' => 'هذا الاسم مطلوب ',
                'phone.required'    => 'البريد الاكلتروني مطلوب ',
                'email.email'    => 'البريد الاكلتروني لابد ان يكون ايميل مثل m@m.com ',
                'phone.unique'    => 'الرقم الموبايل لابد ان يكون رقم غير متشابه مع الاخرين',
                'email.unique'    => 'البريد الالكتروني لابد ان يكون غير متشابه مع الاخر ',
                'email.required'    => 'البريد الاكلتروني مطلوب ',
                'password.required'    => 'مطلوب كلمة السر ',
                'region_id.required'    => ' الحي مطلوب ',
                'city_id.required'    => ' المدينة مطلوب '
            ]
        );

        if($validator->fails()){
            return response()->json([

                'status'   => false,
                'message'    =>'من فضلك ادخل البيانات صحيحة ',
                'errors'    => $validator->errors()


            ], 400);
        }


        $userData = User::create([
            'name' => $request->get('name'),
            'active' => 1,
            'active_message' => 1,
            'email' => $request->get('email'),
            'city_id' => $request->get('city_id'),
            'region_id' => $request->get('region_id'),
            'phone' => $request->get('phone'),
            'password' => Hash::make($request->get('password')),
        ]);

        $package = Package::where('price',0)->first();

        if(!$package){
            // create new package with price 0
            $package =  new Package();
            $package->title = 'الباقة المجانية ';
            $package->number_of_meal = '20';
            $package->price = 0;
            $package->expired_date = 'عند انتهاء الوجبات';
            $package->details = 'الباقة المجانية لكل المستخدمين ';
            $package->created_at = Carbon::now();
            $package->save();
        }

        if($package){
            $packageUser = PackageUser::create([
                'user_id'           => $userData->id,
                'package_id'        => $package->id,
                'pay_number_id'             => null,
                'number_of_meals'   => $package->number_of_meal,
                'current_number_of_meals'   => $package->number_of_meal,
                'status'            => 1,
                'created_at'        => Carbon::now()
            ]);
        }



        $token = JWTAuth::fromUser($userData);

        $status = true;

        $user = [
            'id'            =>$userData->id,
            'name'          =>$userData->name,
            'phone'         =>$userData->phone,
            'image'         =>getImg($userData->image),
            'email'         =>$userData->email,
            'address'         =>$userData->address,
            'weight'         =>$userData->weight,
            'tall'         =>$userData->height,
            'age'         =>$userData->age,
            'type'         =>$userData->type,
            'fitness'         =>$userData->fitness,
            'default_water'         =>$userData->default_water,
            'active'        =>$userData->active,
            'active_message' =>$userData->active_message,
            'city_id' =>$userData->city->id,
            'city_name' =>$userData->city->name,
            'region_id' =>$userData->region->id,
            'region_name' =>$userData->region->name,
            'token' =>$token,
        ];
        $message = " تم تسجيلك بنجاح";


        return response()->json(compact(
            'status','message','user'
        ),200);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $userData = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

            $user = [
                    'id'            =>$userData->id,
                    'name'          =>$userData->name,
                    'phone'         =>$userData->phone,
                    'image'         =>getImg($userData->image),
                    'email'         =>$userData->email,
                    'active'        =>$userData->active,
                    'active_message' =>$userData->active_message,
            ];

        return response()->json(compact('user'));
    }

    public function getUserProfile($id){

        $userData = User::where('id',$id)->first();

        if(!$userData){
            return response()->json([

                'status'   => false,
                'message'    =>'من فضلك ادخل البيانات صحيحة ',
            ], 400);
        }
        $checkUserHasPackage = PackageUser::where('user_id',$userData->id)
            ->where('status',1)
            ->first();

        $check = false;
        if($checkUserHasPackage){
            // check is free package or not
            if($checkUserHasPackage->package->price == 0){
                $check = false;
            }elseif ($checkUserHasPackage->number_of_meals >= $checkUserHasPackage->current_number_of_meals){
                $check = true;
            }
        }

        $user = [
            'id'            =>$userData->id,
            'name'          =>$userData->name,
            'phone'         =>$userData->phone,
            'image'         =>getImg($userData->image),
            'email'         =>$userData->email,
            'address'         =>$userData->address,
            'weight'         =>$userData->weight,
            'tall'         =>$userData->height,
            'age'         =>$userData->age,
            'type'         =>$userData->type,
            'fitness'         =>$userData->fitness,
            'default_water'         =>$userData->default_water,
            'active'        =>$userData->active,
            'has_package'        =>$check,
            'active_message' =>$userData->active_message,
            'city_id' =>$userData->city->id,
            'city_name' =>$userData->city->name,
            'region_id' =>$userData->region->id,
            'region_name' =>$userData->region->name,
        ];






        // sports cals to to day

        $SportUsersToday = SportUser::where('user_id',$userData->id)->whereDate('created_at', date(today()))->get();

        $sportsCalsToday = 0;
        if($SportUsersToday){
            foreach ($SportUsersToday as $item){
                $sportsCalsToday = $sportsCalsToday + $item->sport->cals;
            }
        }




        // products cals to to day

        $userCartsId =  Cart::distinct('id')->where('user_id',$userData->id)->where('status','opening')->whereDate('created_at', date(today()))->pluck('id')->toArray();


        $cartProduct = CartProduct::distinct('cart_id')->find($userCartsId);
        $productsCals = 0;

        if($cartProduct){
            foreach ($cartProduct as $item){
                $productsCals = $productsCals + $item->product->cals;
            }
        }



        $user_default_calcs = $userData->default_cals;


        $calsData = [
            'user_default_cals' => $user_default_calcs,
            'user_product_cals'      => $productsCals,
            'user_sports_cals'           => $sportsCalsToday,
            'today_all_cals'              => $user_default_calcs + $productsCals - $sportsCalsToday

        ];

        return response()->json(compact(
            'user',
            'calsData'
        ),200);


    }

    public function update(Request $request){
            $user = \JWTAuth::user();

            if(!$user){
                return response()->json([

                    'status'   => false,
                    'message'    =>'من ضلك قم بتسجيل الدخول',

                ], 400);
            }
            $user_id = $user->id;
            
            if($request->hasFile('image') && !empty($request->file('image'))){

                // update user with image

                //$file = $request->file('image');

                $imgName = uploader($request->file('image'));
                $user->name = ($request->get('name') != null) ? $request->get('name') :  $user->name;
                $user->email = ($request->get('email') != null) ? $request->get('email') :  $user->email;
                $user->address = ($request->get('address') != null) ? $request->get('address') :  $user->address;
                $user->weight = ($request->get('weight') != null) ? $request->get('weight') :  $user->weight;
                $user->height = ($request->get('height') != null) ? $request->get('height') :  $user->height;
                $user->age = ($request->get('age') != null) ? $request->get('age') :  $user->age;
                $user->type = ($request->get('type') != null) ? $request->get('type') :  $user->type;
                $user->fitness = ($request->get('fitness') != null) ? $request->get('fitness') :  $user->fitness;

                $user->image = $imgName;

                if($user->save()){
                    $user = User::findOrFail($user_id);
                    $message = "تم التحديث بنجاح";
                    $status = true;
                    return response()->json(compact('status','message','user'),200);
                }else{
                    $status = false;
                    return response()->json([
                        'status'    => false,
                        'message'     => 'من فضلك ادخل البيانات صحصحية'
                    ]);
                }


            }
            else{
                //update the user without image

                $user->name = ($request->get('name') != null) ? $request->get('name') :  $user->name;
                $user->email = ($request->get('email') != null) ? $request->get('email') :  $user->email;
                $user->address = ($request->get('address') != null) ? $request->get('address') :  $user->address;
                $user->weight = ($request->get('weight') != null) ? $request->get('weight') :  $user->weight;
                $user->height = ($request->get('height') != null) ? $request->get('height') :  $user->height;
                $user->age = ($request->get('age') != null) ? $request->get('age') :  $user->age;
                $user->type = ($request->get('type') != null) ? $request->get('type') :  $user->type;
                $user->fitness = ($request->get('fitness') != null) ? $request->get('fitness') :  $user->fitness;

                if($user->save()){
                    $status = true;
                    $message = "تم تحديث البروفايل بنجاح";
                    $user = User::findOrFail($user_id);
                    return response()->json(compact('status','message','user'),200);
                }else{
                    return response()->json([
                        'status'    => false,
                        'message'     => 'من فضلك ادخل البيانات صحصحية'
                    ]);
                }
            }

            //return response()->json(compact('user','token'),201);

        }


    public function updatePassword(Request $request){

            $user = JWTAuth::user();


            $validator = Validator::make($request->all(), [
                'old_pass' => 'required',
                'password' => 'required|confirmed|min:6',
            ]);


            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }

            $old_password = $request->get('old_pass');


            if ((Hash::check($old_password, $user->password))) {
                // The passwords matches
                $user->password =  Hash::make($request->get('password'));
                $user->save();

                return response()->json([
                    'success' => 'Updated password successfully'
                ],'200');

            }else{
                return response()->json([
                    'error' => 'Your current password does not matches with the password you provided. Please try again'
                ],'400');
            }
        }

    public function all(){
            $users = DB::table('users')
            ->select('users.*')
            ->join('socials', 'socials.user_id', '=', 'users.id')
            ->distinct('users.id')
            ->orderBy('users.id')
            ->get();
            $users = new UserResource($users);
            return $this->apiResponse($users);
        }

    public function addSport(Request $request){
        $user = JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        $validator = Validator::make($request->all(), [
            'sport_id' => 'required',
            'played_time' => 'required|time',
        ]);

        // get sport default time

        $sport = Sport::where('id',$request->get('sport_id'))->first();

        if(!$sport){
            return response()->json([

                'status'   => false,
                'message'    =>'هذه الرياضة غير موجوده اختر رياضة صحيحه ',

            ], 400);
        }
        $userId = JWTAuth::user()->id;

        $sportId = $request->get('sport_id');

        $today = today()->toDateTimeString();
        // insert into sport_user table



        // check user play this sport today or not

        $sport_user = SportUser::where('user_id',$userId)
            ->where('sport_id',$sport->id)->whereDate('created_at',date(today()))->first();


        $sportDefaultTime = $sport->default_sport_time;
        $played_time = $request->get('played_time');

        if($sport_user){

                // add time to default time
            $secs = strtotime($sport_user->spend_time)-strtotime("00:00:00");
            $total = date("H:i:s",strtotime($played_time) + $secs);


            if($total > $sportDefaultTime){
                $sport_user->spend_time = $sportDefaultTime;
                $sport_user->save();
            }else{
                $sport_user->spend_time = $total;
                $sport_user->save();
            }

        }

        else{
            if($played_time > $sportDefaultTime){
                $played_time =  $sportDefaultTime;
            }
            $sport_row = DB::insert("INSERT INTO sport_user SET user_id = '$userId',sport_id = '$sportId',spend_time = 
            '$played_time', created_at = '$today'");
        }
        //

            return response()->json([
                'status' =>true,
                'message'   => 'تم اضافة الرياضة بنجاح'
            ],'200');

    }

    public function addPackage(Request $request){
        $user = JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        $validator = Validator::make($request->all(), [
            'package_id' => 'required',
            'code' => 'required',
        ]);

        // get sport default time

        if($validator->fails()){
            return response()->json(['لم نتمكن من اضافة الباقة راجع البيانات '],400);
        }


        // if user have package and it active , user can't add the new package

        $oldPackage = PackageUser::where('user_id',$user->id)
            ->where('pay_number_id','!=',null)
            ->where('status',1)->first();





        if($oldPackage){
            return response()->json([
                'status' =>true,
                'message'   => 'انت بالفعل مشترك في باقة لابد ان تنهي الباقة المفعله'
            ],'200');
        }

        $package = Package::where('id',$request->get('package_id'))->first();

        if(!$package){
            return response()->json([

                'status'   => false,
                'message'    =>'هذه الباقة غير موجوده اختر باقة صحيحة',

            ], 400);
        }
        $code = $request->get('code');
        $code = PayNumber::where('generated_number',$code)->first();


        if($package && $code){
            $status = 1;
            $created_at = Carbon::now();

            $packageUser = new PackageUser();

            $packageUser->user_id = $user->id;


            $packageUser->pay_number_id = $code->id;


            $packageUser->package_id = $package->id;


            $packageUser->number_of_meals = $package->number_of_meal;

            $packageUser->current_number_of_meals = $package->number_of_meal;

            $packageUser->status = $status;

            $packageUser->created_at = $created_at;

            //
            if($packageUser->save()){
                return response()->json([
                    'status' => true,
                    'message' => 'تم اضافة الباقة الادمن ومفعله الان  '
                ],'200');
            }else {
                return response()->json([
                    'status'    => false,
                    'message' => 'حطأ في اضافة الباقة من فضلك راجع البايانات'
                ],'400');
            }

        } else{

            return response()->json([
                'status'    => false,
                'message' => 'حطأ في اضافة الباقة من فضلك اختار الباقة بشكل صحيح او ادخل الكود صحيح '
            ],'400');
        }
    }


    public function getUserDate(){
        $user = \JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        $data = DB::table('user_sports')->select('id','user_id','created_at')->where('user_id',$user->id)->groupBy(DB::raw('DATE(created_at)'))->groupBy('user_id')->get();
        dd($data);
    }


    // get user active Packages


    public function getPackagesForLoginUser(){
        $user = JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        $q = PackageUser::where('user_id',$user->id)->where('pay_number_id','!=',null)->where('status','1')->first();

        if($q){
            $packageUser = [
                'id'                    =>$q->id,
                'user'                  =>$q->user,
                'title'                 =>$q->package->title,
                'price'                 =>$q->package->price,
                'code'                  =>($q->pay_number_id)? $q->cartNumber : null,
                'details'               =>$q->package->details,
                'expired_date'          =>$q->package->expired_date,
                'number_of_meals'       =>$q->package->number_of_meal,
                'current_number_of_meals'=>$q->current_number_of_meals,
            ];
            return response()->json([
                'status' => true,
                'message' => 'باقة المستخدم',
                'package' => $packageUser
            ],'200');
        }else{
            return response()->json([
                'status' => false,
                'message' => 'من فضلك اشترك في الباقات المحددة انت غير مشترك بها '
            ],'400');
        }


        //$packageUser = new PackageUserResource($packageUser);
        //return $this->apiResponse($packageUser);
    }

    public function getTodayPlayedSportsForUser(){

        $user = JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        $user_id = $user->id;
        // get today sport that user play them.
        $today_sports = SportUser::where('user_id',$user_id)
            ->where('created_at',today()->toDateString())->get();
        $played_today_sports = new SportUserResource($today_sports);
        return $this->apiResponse($played_today_sports);


    }
    public function getPlayedSportsForUser(){
        $user = JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        $user_id = $user->id;
        // get today sport that user play them.
        $today_sports = SportUser::where('user_id',$user_id)
            ->orderBy('created_at','DESC')->get();
        $played_today_sports = new SportUserResource($today_sports);
        return $this->apiResponse($played_today_sports);

    }

    public function getTodayCupOfWaterForUser(){

        $user = JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }

        $user_id = $user->id;

        $user_cup_of_waters = Water::where('user_id',$user_id)
            ->whereDate('created_at',date(today()))->get();

        $user_cup_of_waters = new WatersResource($user_cup_of_waters);
        
        return $this->apiResponse($user_cup_of_waters);

    }
    public function getSingleSport($id){

        $user = \JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        $sport_data = SportUser::where('user_id',$user->id)->where('sport_id',$id)->first();
        $sport = [];
        if($sport_data){
            $sport = [
                'id'            =>$sport_data->id,
                'title'          =>$sport_data->sport->title,
                'image'         =>getImg($sport_data->sport->image),
                'default_time'         =>$sport_data->sport->default_sport_time,
                'spend_time'         =>$sport_data->spend_time,
            ];


            return response()->json([
                'status'    => true,
                'sport'    => $sport,
                'message' => 'user sport data'
            ],'200');
        }else{
            $sport = Sport::where('id',$id)->first();

            if($sport){
                $sport = [
                    'id'            =>$sport->id,
                    'title'          =>$sport->title,
                    'image'         =>getImg($sport->image),
                    'default_time'         =>$sport->default_sport_time,
                    'spend_time'         =>"00:00:00",
                ];


                return response()->json([
                    'status'    => true,
                    'sport'    => $sport,
                    'message' => 'تفاصيل الرياضة'
                ],'200');
            }else{
                return response()->json([
                    'status'    => false,
                    'message' => 'الرياضة هذه غير موجوة'
                ],'404');
            }


        }
    }

    public function getSocialsForUser($user_id){
        $user = User::where('id',$user_id)->first();
        if(!$user){
            return response()->json([
                'status'    => false,
                'message' => 'هذا المستخدم غير موجود '
            ],'400');

        }
        $socialsData = Social::where('user_id',$user_id)->get();
        $socials = new SocialResource($socialsData);
        return $this->apiResponse($socials);
    }

    public function getSocialsForLoginUser(){
        $user = JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        $socialsData = Social::where('user_id',$user->id)->get();
        $socials = new SocialResource($socialsData);
        return $this->apiResponse($socials);
    }

    public function addSocials(Request $request){

        $user = JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }

        $title = $request->get('title');
        $social = new Social();


// update user with image

                //$file = $request->file('image');

                if($request->hasFile('data_file')){

                    $file = $request->file('data_file');
                    //$filename = $file->getClientOriginalName();
                    $filename = time() . '.' . $file->getClientOriginalExtension();
                    $path = public_path().'/uploads/';

                    $file->move($path, $filename);
                    $social->data = $filename;
                }
        // check this is image
       


        $social->user_id = $user->id;
        $social->title = $title;
        $social->created_at = today()->toDateTimeString();
        if($social->save()){
            return response()->json([
                'status'    => true,
                'message' => 'تم اضافة المنشوار بنجاح'
            ],'200');
        }else{
            return response()->json([
                'status'    => false,
                'message' => 'خطأ في اضافة المنشوار'
            ],'400');
        }



    }

    public function addCupOfWater(Request $request){

        $user = JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }

        $cup_of_water = $request->get('cup_of_water');

        // select if user have today cup of water

        $cup = Water::where('user_id',$user->id)->whereDate('created_at',
            date(today()->toDateString()))->first();

        if($cup){

            $cup->cup_of_water = (integer)$cup->cup_of_water + $cup_of_water;

            if($cup->save()){
                return response()->json([
                    'status'    => true,
                    'message'   => 'تم اضافة كوب من الماء لحسابك الان '
                ],201);
            }
        }else{
            $cup = new Water();
            $cup->user_id = $user->id;
            $cup->cup_of_water = $cup_of_water;
            $cup->created_at = Carbon::now();

            if($cup->save()){
                return response()->json([
                    'status'    => true,
                    'message'   => 'تم اضافة كوب من الماء لحسابك الان '
                ],201);
            }
        }


    }

    public function remove_social($id){

        if(JWTAuth::user()){

            $user_id = JWTAuth::user()->id;

            $social = Social::where('user_id',$user_id)->where('id',$id)->first();
            if($social){
                $social->delete();
                return response()->json([
                    'status'    => true,
                    'message' => 'تم مسح المنشوار بنجاح'
                ],'200');
            }
            else
            {

                return response()->json([
                    'status'    => false,
                    'message' => 'لا نتمكن من مسح المنشوار الان  '
                ],'200');
            }

        }else{
            return response()->json([
                'status'    => false,
                'message' => 'login required'
            ],'200');
        }
    }

    public function getList($status){
        $user = \JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        $list = Cart::where('user_id',$user->id)->where('status',$status)->get();
        $carts =  new CartResource($list);
        return $this->apiResponse($carts);

    }
    public function my_cart($order_type){

        $user = JWTAuth::user();
        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        $cart_data = DB::table('carts')
            ->select(
                'carts.id',
                'carts.user_id',
                'carts.product_id',
                'carts.quantity',
                'carts.status',
                'users.name',
                'users.phone',
                'products.title',
                'products.price',
                'products.image',
                'products.cals',
                'carts.created_at'
                )
            ->join('users','carts.user_id','=','users.id')
            ->join('products','carts.product_id','=','products.id')
            ->where(['carts.status'=>$order_type,
                'carts.user_id'=>$user->id])
            ->get();


        //dd($cart_data);

        $cart_data = new CartProductResource($cart_data);

        return $this->apiResponse($cart_data);

    }

    public function add_cart(Request $request){

        $validator = Validator::make($request->all(), [
            'product_id' => 'required|int',
            'quantity' => 'required|int|max:20'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }


        $user = JWTAuth::user();
        if(!$user){
            return response()->json([
                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }

        $user_id = JWTAuth::user()->id;
        // check the user has package
        $userPackge = PackageUser::where('user_id',$user_id)
            ->where('status',1)->orderBy('created_at','DESC')->first();

        if($userPackge){
            // check the number of meals with current number
            if($userPackge->current_number_of_meals != 0){
                $userPackge->current_number_of_meals =
                    $userPackge->current_number_of_meals - 1;
                $userPackge->save();
            }else{
                $userPackge->status = 0;
                $userPackge->save();
                return response()->json([
                    'status'    => false,
                    'message' => 'انتهت الباقة الخاصة بك من فضلك اضف باقة جديدة وتواصل مع الادمن '
                ],'200');
            }

        }
        else{
            return response()->json([
                'status'    => false,
                'message' => 'من فضلك اشترك في الباقات المحددة '
            ],'200');
        }

        $quantity = $request->get('quantity');
        $product_id = $request->get('product_id');


        // check user has Kay or not



            $cart = new Cart();
            $cart->user_id = $user_id;
            $cart->product_id = $product_id;
            $cart->quantity = $quantity;
            $cart->status   = 'in_cart';
            $cart->created_at = today()->toDateTimeString();

        if($cart->save()){

            return response()->json([
                'status'    => true,
                'message' => 'تم اضافة العنصر الي العربة'
            ],'200');
        }else{
            return response()->json([
                'status'    => false,
                'message' => 'لم نتمكن من اضافة العنصر الي العربة'
            ],'200');
        }


    }
    public function processCarts(Request $request){

        $user = \JWTAuth::user();

        if(!$user){
            return response()->json([

                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }

        $carts = Cart::where('user_id',$user->id)->where('status','in_cart')->get();

        if($carts){

            foreach ($carts as $cart){
                $cart->status = 'pending';
                $cart->save();
            }
            return response()->json([
                'status'    => true,
                'message' => 'الادمن سوف يراجع بيانات العربة للموافقة او الرفض '
            ],200);
        }else{
            return response()->json([
                'status'    => false,
                'message' => 'العربة فارغة , من فضلك اضف عناصر بها '
            ],200);
        }


    }



    public function remove_cart($id){

        if(JWTAuth::user()){

            $user_id = JWTAuth::user()->id;

            $cart = Cart::where('user_id',$user_id)->where('id',$id)->first();


            $cart->delete();

            return response()->json([
                'status'    => true,
                'message' => 'تم مسح العنصر من العربة'
            ],'200');

        }else{
            return response()->json([
                'status'    => false,
                'message' => 'خطأ في مسح العربة الان راجع بياناتك'
            ],'200');
        }
    }

    public function update_cart(Request $request){

        if(JWTAuth::user()){

            $cart_id = $request->get('cart_id');
            $quantity = $request->get('quantity');

            $user_id = JWTAuth::user()->id;

            $cart = Cart::where('user_id',$user_id)->where('id',$cart_id)->first();

            if($cart){
                $cart->quantity = $quantity;
                if($cart->save()){
                    return response()->json([
                        'status'    => true,
                        'message' => 'تم تحديث العربة بنجاح'
                    ],'200');
                }
            }else{

                return response()->json([
                    'status'    => false,
                    'message' => 'you not have the cart '
                ],'200');

            }

        }else{
            return response()->json([
                'status'    => false,
                'message' => 'من فضلك قم بتسجيل الدخول '
            ],'200');
        }
    }

    public function my_chats(){
        $user = JWTAuth::user();
        if(!$user){
            return response()->json([
                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        $chats_users =Chat::groupBy('receiver','sender')->where('sender',$user->id)->orWhere('receiver',$user->id)->orderBy('created_at')->get();
        $users = new ChatResource($chats_users);
        return $this->apiResponse($users);
    }

    public function chatWithUser($user_id){
        $user = JWTAuth::user();
        if(!$user){
                return response()->json([
                    'status'   => false,
                    'message'    =>'من ضلك قم بتسجيل الدخول',

                ], 400);
            }
          $user2 = $user->id;
          $chats_users = Chat::where('sender','=',$user2)->where('receiver','=',$user_id)
                               ->orWhere('sender','=',$user_id)->where('receiver','=',$user2)->get();
 //       $chats_users = DB::select("SELECT * FROM chats WHERE (sender = '$user->id' AND receiver = '$user_id' ) OR (sender = '$user_id' AND receiver = '$user->id')");
          $users = new ChatMessageResource($chats_users);
          return $this->apiResponse($users);
    }

    public function sendMessageToUser(Request $request){
        $chat = new Chat();
        $sender = JWTAuth::user();
        if(!$sender){
            return response()->json([
                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }


        $validator = Validator::make($request->all(), [
            'user_id' => 'required|int',
            'message' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }


        $message = $request->get('message');
        $user_id = $request->get('user_id');
        $receiver = User::where('id',$user_id)->first();

        if(!$receiver){
            return response()->json([
                'status'   => false,
                'message'    =>'من ترسل له ليس موجود ',

            ], 400);
        }

        $userPackge = DB::table('package_users')
            ->join('packages', 'package_users.package_id', '=', 'packages.id')
            ->select('package_users.*', 'packages.*')
            ->where('package_users.status',1)
            ->where('packages.price','>',0)
            ->first();

        if(!$userPackge){
            return response()->json([
                'status'    => false,
                'message' => 'من فضلك اشترك في الباقات المحددة للتحدث مع الادمن'
            ],'200');
        }

        if($sender->active_message == 1){
            if($receiver->active_message == 1){
                // you can send a message
                if($request->hasFile('attachment') && !empty($request->file('attachment'))){
                    $file = $request->file('attachment');
                    //$filename = $file->getClientOriginalName();
                    $filename = time() . '.' . $file->getClientOriginalExtension();
                    $path = public_path().'/uploads/';

                    $file->move($path, $filename);
                    $attachment = $filename;

                }else{
                    $attachment = null;
                }

                $chat->has_file = $attachment;
                $chat->message = $message;
                $chat->sender = $sender->id;
                $chat->receiver = $receiver->id;
                $chat->has_file = $attachment;
                $chat->created_at = today()->toDateTimeString();
                $chat->updated_at = null;
                if($chat->save()){
                    return response()->json([
                        'status'    => true,
                        'message' => 'تم ارسال الرسالة بنجاح'
                    ],'200');
                }else{
                    return response()->json([
                        'status'    => false,
                        'message' => 'خطأ في ارسال الرسالة '
                    ],'200');

                }
            }else{
                return response()->json([
                    'status'    => false,
                    'message' => 'receiver can not start chat with other users  '
                ],'400');
            }
        }else{
            return response()->json([
                'status'    => false,
                'message' => 'sender can not start chat with other users  '
            ],'400');
        }



    }

    public function likeOrLoveSocial($social_id,$type){

        //
        $user = \JWTAuth::user();


        if(!$user){
            return response()->json([
                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        if($user){
            // check if user like or love the social
            $social = Social::where('id',$social_id)->first();
            if($social){

                $like = Like::where('user_id',$user->id)->where('social_id',$social_id)->first();
                if($like){
                    // yes user like or love this post
                    return response()->json([
                        'status'    => false,
                        'message'   => 'تم عمل لايك او حب من قبل '
                    ],400);
                }else{

                    $like = new Like();
                    $like->user_id = $user->id;
                    $like->social_id = $social_id;
                    $like->type_of_like = $type;
                    $like->created_at = Carbon::now();

                    if($like->save()){

                        if($like->type == 1)
                        {
                            $social->likes = (integer) ($social->likes + 1);
                            $social->save();
                        }
                        else{
                            $social->loves = (integer) ($social->loves + 1);
                            $social->save();
                        }


                        return response()->json([

                            'status'    => true,
                            'message'   => 'تم الاعجاب بالمنشوار '

                        ],201);

                    }
                }

            }
            else{

                return response()->json([
                    'status'    => false,
                    'message'   => 'هذا المنشوار لم يعد موجود حاليا '
                ],400);

            }
        }
        else {
            return response()->json([
                'status'    => false,
                'message'   => 'راجع تسجيل الدخول الان  '
            ],400);
        }

    }

    // get admin
    public function getAdmin(){
        $admin = User::where('is_admin',1)->orderBy(DB::raw('RAND()'))->first();
        return response([
            'status'    => true,
            'admin'     => $admin
        ],200);


    }

    // today report
    public function getTodayReport(){
        $user = JWTAuth::user();

        if(!$user){
            return response()->json([
                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        // sports cals to to day

        $SportUsersToday =
            SportUser::where('user_id',$user->id)->whereDate('created_at',
                date(today()))->get();

        $user_cup_of_waters = Water::where('user_id',$user->id)->whereDate('created_at',date(today()))->first();



        $sportsCalsToday = 0;

        if($SportUsersToday){
            foreach ($SportUsersToday as $item){
                $sportsCalsToday = $sportsCalsToday + $item->sport->cals;
            }
        }


        // products cals to to day

        $cartProduct =  Cart::where('user_id',$user->id)->where('status','opening')->whereDate('created_at',
            date(today()))->get();


        //$cartProduct = CartProduct::distinct('cart_id')->find($userCartsId);


        $productsCals = 0;

        $products = [];
        if($cartProduct){
            foreach ($cartProduct as $item){
                $products[] = [
                  'product_id'       => $item->product->id,
                  'product_cals'     => $item->product->cals,
                  'product_title'    => $item->product->title,
                  'product_image'    => getImg($item->product->image),
                ];

            }
            foreach ($cartProduct as $item){
                $productsCals = $productsCals + $item->product->cals;
            }
        }else{
            $products = null;
        }





        $user_default_calcs = $user->default_cals;


        $sports = [];

        if($SportUsersToday){
            foreach ($SportUsersToday as $sport){
                $sports[] = [

                    "id"=> $sport->sport_id,
                    "title"=> $sport->sport->title,
                    "default_sport_time"=> $sport->sport->default_sport_time,
                    "cals"=> $sport->sport->cals,
                    "image"=> getImg($sport->sport->image),
                    "spend_time"=> $sport->spend_time,


                ];
            }
        }




        return response()->json([

            'status' => true,
            'message' => "التقرير اليومي للعميل",
            'sports' => $sports,
            'products' => $products,
            'user_default_cals' => $user_default_calcs,
            'user_product_cals'      => $productsCals,
            'user_sports_cals'           => $sportsCalsToday,
            'user_default_cup_of_water'           => $user->default_water,
            'user_water_cup'           => ($user_cup_of_waters)? $user_cup_of_waters->cup_of_water  : 0,
            'today_all_cals'              => $user_default_calcs + $productsCals - $sportsCalsToday

        ],200);







    }

    // report based on date
    public function getDayReport(Request $request){
        $user = JWTAuth::user();
        if(!$user){
            return response()->json([
                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        $date = $request->get('date');
        // sports cals to to day
        $SportUsersToday = SportUser::where('user_id',$user->id)->whereDate('created_at', date($date))->get();
        $sportsCalsToday = 0;


        $user_cup_of_waters = Water::where('user_id',$user->id)->whereDate('created_at',date($date))->first();

        if($SportUsersToday){
            foreach ($SportUsersToday as $item){
                $sportsCalsToday = $sportsCalsToday + $item->sport->cals;
            }
        }




        // products cals to to day


        // products cals to to day

        $cartProduct =  Cart::where('user_id',$user->id)->where('status','opening')->whereDate('created_at',
            date(today()))->get();

        $productsCals = 0;

        $products = [];
        if($cartProduct){

            foreach ($cartProduct as $item){
                $products[] = [
                    'product_id'       => $item->product->id,
                    'product_cals'     => $item->product->cals,
                    'product_title'    => $item->product->title,
                    'product_image'    => getImg($item->product->image),
                ];

            }


            foreach ($cartProduct as $item){
                $productsCals = $productsCals + $item->product->cals;
            }
        }else{
            $products = null;
        }


        $user_default_calcs = $user->default_cals;

        $sports = [];

        if($SportUsersToday){
            foreach ($SportUsersToday as $sport){
                $sports[] = [

                    "id"=> $sport->sport_id,
                    "title"=> $sport->sport->title,
                    "default_sport_time"=> $sport->sport->default_sport_time,
                    "cals"=> $sport->sport->cals,
                    "image"=> getImg($sport->sport->image),
                    "spend_time"=> $sport->spend_time,


                ];

            }
        }


        return response()->json([

            'status' => true,
            'message' => "التقرير اليومي للعميل",
            'sports' => $sports,
            'products' => $products,
            'user_default_cals' => $user_default_calcs,
            'user_product_cals'      => $productsCals,
            'user_sports_cals'           => $sportsCalsToday,
            'user_default_cup_of_water'           => $user->default_water,
            'user_water_cup'           => ($user_cup_of_waters)? $user_cup_of_waters->cup_of_water  : 0,
            'day_all_cals'              => $user_default_calcs + $productsCals - $sportsCalsToday

        ],200);




    }
    // get days
    public function  getDaysThatUserPlaySports($user_id){


        $created_waters_dates =  DB::table('waters')
            ->select(DB::raw('DATE(created_at) as date'))
            ->where('user_id',$user_id)
            ->groupBy('date');

        $created_sports_at_days = DB::table('sport_user')
            ->select(DB::raw('DATE(created_at) as date'))
            ->where('user_id',$user_id)
            ->union($created_waters_dates)->pluck('date')->toArray();

        return response()->json([
            'status' => true,
            'message' => "تواريخ المستخدم ",
            'dates' => $created_sports_at_days
        ],200);



    }


    public function countNumbersInCart(){

        $user = \JWTAuth::user();


        if(!$user){
            return response()->json([
                'status'   => false,
                'message'    =>'من ضلك قم بتسجيل الدخول',

            ], 400);
        }
        // get number of cart in cart

        $cartsNumbers = Cart::where('user_id',$user->id)->where('status','in_cart')->count();

        if($cartsNumbers){
            return response()->json([
                'status' => true,
                'message' => "عدد العناصر داخل البطاقة ",
                'cart_numbers' => $cartsNumbers
            ],200);
        }else{
            return response()->json([
                'status' => false,
                'message' => "بطاقتك فارغة",
                'cart_numbers' => 0
            ],400   );
        }
    }

}
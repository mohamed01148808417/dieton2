<?php

namespace App\Http\Controllers\Admin;

use App\Meal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MealsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $meals =  Meal::orderBy('created_at','DESC')->paginate(50);
        return view('admin.meals.index')->with('meals',$meals);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       return view('admin.meals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(),[
            'name'=>'required',
        ]);


        if ($validator->fails())
        {
            session()->flash('error','خطأ في الاسم');
            return back();
        }

         $meal = new Meal();

        $meal->name = $request->name;
        $meal->created_at = Carbon::now();
        if($meal->save()){
            session()->flash('success','تم الحفظ  ');
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $meal = Meal::findOrFail($id);

        return view('admin.meals.edit')->with('meal',$meal);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validator = \Validator::make($request->all(),[
            'name'=>'required',
        ]);


        if ($validator->fails())
        {
            session()->flash('error','خطأ في الاسم');
            return back();
        }

        $meal = Meal::findOrFail($id);

        $meal->name = $request->name;
        $meal->updated_at = Carbon::now();
        if($meal->save()){
            session()->flash('success','تم التعديل  ');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $meal = Meal::findOrFail($id);

        if($meal->delete()){
            session()->flash('info','تم المسج  ');
        }

        return back();
    }
}

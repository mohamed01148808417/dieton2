<?php

namespace App\Http\Controllers\Admin;

use App\Package;
use App\PackageUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $packages =  Package::orderBy('created_at','DESC')->paginate(50);
        return view('admin.packages.index')->with('packages',$packages);
    }


    public function userPackages($user_id){
        $packages =  PackageUser::orderBy('created_at','DESC')->where('user_id',$user_id)->paginate(50);
        return view('admin.users.packages')->with('packages',$packages);
    }


    //
    public function activePackages(){

        $packages =  PackageUser::orderBy('created_at','DESC')->where('status',1)->paginate(50);
        return view('admin.packages.active')->with('packages',$packages);
    }

    public function unActivePackages(){
        $packages =  PackageUser::orderBy('created_at','DESC')->where('status',0)->paginate(50);
        return view('admin.packages.un_active')->with('packages',$packages);
    }


    public function destroy_package($id){


        $packageUser = PackageUser::findOrFail($id);

        if($packageUser->delete()){
            session()->flash('success','تم مسح الباقة بنجاح  ');
            return back();
        }

    }

    public  function active_package($id){

        $packageUser = PackageUser::findOrFail($id);

        if($packageUser){
            $packageUser->status = 1;
            if($packageUser->save()){
                session()->flash('success','تم تفعيل الباقة بنجاح  ');
                return back();
            }
        }else{
            session()->flash('success','خطاء في تفعيل الباقة ');
            return back();

        }


    }

    public function un_active_package($id){

        $packageUser = PackageUser::findOrFail($id);

        if($packageUser){
            $packageUser->status = 0;
            $packageUser->current_number_of_meals = 0;

            if($packageUser->save()){
                session()->flash('success','تم الغاء الباقة بنجاح  ');
                return back();
            }
        }else{
            session()->flash('success','خطاء في الغاء الباقة ');
            return back();

        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.packages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(),[
            'title'=>'required',
            'number_of_meal'=>'required',
            'price'=>'required',
            'expired_date'=>'required',
            'details'=>'required',
        ]);

        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');
            return back();
        }

        $package = new Package();

        $package->title = $request->title;
        $package->number_of_meal = $request->number_of_meal;
        $package->price = $request->price;
        $package->expired_date = $request->expired_date;
        $package->details = $request->details;
        $package->created_at = Carbon::now();
        if($package->save()){
            session()->flash('success','تم الحفظ  ');
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $package = Package::findOrFail($id);

        return view('admin.packages.edit')->with('package',$package);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validator = \Validator::make($request->all(),[
            'title'=>'required',
            'number_of_meal'=>'required',
            'price'=>'required',
            'expired_date'=>'required',
            'details'=>'required',
        ]);


        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');
            return back();
        }

        $package = Package::findOrFail($id);

        $package->title = $request->title;
        $package->number_of_meal = $request->number_of_meal;
        $package->price = $request->price;
        $package->expired_date = $request->expired_date;
        $package->details = $request->details;
        $package->updated_at = Carbon::now();
        if($package->save()){
            session()->flash('success','تم التعديل  ');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $package = Package::findOrFail($id);

        if($package->delete()){
            session()->flash('info','تم المسج  ');
        }

        return back();
    }
}

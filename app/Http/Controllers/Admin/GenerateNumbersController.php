<?php

namespace App\Http\Controllers\Admin;

use App\PayNumber;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GenerateNumbersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $PayNumbers =  PayNumber::orderBy('created_at','DESC')->paginate(50);
        return view('admin.PayNumbers.index')->with('PayNumbers',$PayNumbers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       return view('admin.PayNumbers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(),[
            'price'=>'required',
        ]);


        if ($validator->fails())
        {
            session()->flash('error','خطأ في السعر');
            return back();
        }

         $PayNumber = new PayNumber();

        $PayNumber->price = $request->price;
        $PayNumber->generated_number = generateRandomNumber();
        $PayNumber->created_at = Carbon::now();
        if($PayNumber->save()){
            session()->flash('success','تم الحفظ  ');
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $PayNumber = PayNumber::findOrFail($id);

        return view('admin.PayNumbers.edit')->with('PayNumber',$PayNumber);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validator = \Validator::make($request->all(),[
            'price'=>'required',
        ]);


        if ($validator->fails())
        {
            session()->flash('error','خطأ في السعر');
            return back();
        }

        $PayNumber = PayNumber::findOrFail($id);

        $PayNumber->price = $request->name;
        $PayNumber->generated_number = generateRandomNumber();
        $PayNumber->updated_at = Carbon::now();
        if($PayNumber->save()){
            session()->flash('success','تم التعديل  ');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $PayNumber = PayNumber::findOrFail($id);

        if($PayNumber->delete()){
            session()->flash('info','تم المسج  ');
        }

        return back();
    }
}

<?php

namespace App\Http\Controllers\Admin;
use App\Meal;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products =  Product::orderBy('created_at','DESC')->paginate(50);
        return view('admin.products.index')->with('products',$products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $meals = Meal::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE)->pluck('name', 'id');
       return view('admin.products.create')->with('meals',$meals);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(),[
            'title'=>'required',
            'meal_id'=>'required',
            'price'=>'required',
            'cals'=>'required',
            'active'=>'required',
            'desc'=>'required',
            'image'=>'required|mimes:jpeg,jpg,bmp,png|max:2048',
        ]);




        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');
            return back();
        }

        $product = new Product();

        if ($request->has('image'))
        {
            $product->image = uploader($request->image);
        }
        $product->title = $request->title;
        $product->meal_id = $request->meal_id;
        $product->price = $request->price;
        $product->cals = $request->cals;
        $product->active = $request->active;
        $product->desc = $request->desc;
        $product->created_at = Carbon::now();
        if($product->save()){
            session()->flash('success','تم الحفظ  ');
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $meals = Meal::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE)->pluck('name', 'id');
        $product = Product::findOrFail($id);
        return view('admin.products.edit')->with('product',$product)->with('meals',$meals);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validator = \Validator::make($request->all(),[
            'title'=>'required',
            'meal_id'=>'required',
            'price'=>'required',
            'cals'=>'required',
            'active'=>'required',
            'desc'=>'required',
        ]);




        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');
            return back();
        }

        $product = Product::findOrFail($id);

        if ($request->has('image'))
        {
            $product->image = uploader($request->image);
        }
        $product->title = $request->title;
        $product->meal_id = $request->meal_id;
        $product->price = $request->price;
        $product->cals = $request->cals;
        $product->active = $request->active;
        $product->desc = $request->desc;
        $product->created_at = Carbon::now();

        if($product->save()){
            session()->flash('success','تم التعديل  ');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        if($product->delete()){
            session()->flash('info','تم المسج  ');
        }

        return back();
    }
}

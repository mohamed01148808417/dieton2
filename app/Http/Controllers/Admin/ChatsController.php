<?php

namespace App\Http\Controllers\Admin;

use App\Chat;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ChatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $meals =  Chat::where('receiver','!=','10')->orderBy('created_at','DESC')->paginate(50);
        return view('admin.chats.index')->with('chats',$meals);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       $user_id = $_GET['id'];
       $user = User::findOrFail($user_id);
       return view('admin.chats.create')->with('user',$user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $admin = Auth::user();
        $validator = \Validator::make($request->all(),[
            'message'=>'required',
        ]);


        if ($validator->fails())
        {
            session()->flash('error','خطأ في الرسالة');
            return back();
        }

         $chat = new Chat();

        $chat->sender = $admin->id;
        $chat->receiver = $request->user_id;
        $chat->message = $request->message;
        $chat->created_at = Carbon::now();
        if($chat->save()){
            session()->flash('success','تم ارسال الرسالة ');
        }

        return back();
    }

    public function destroy($id)
    {
        //
        $meal = Chat::findOrFail($id);

        if($meal->delete()){
            session()->flash('info','تم المسج  ');
        }

        return back();
    }
}

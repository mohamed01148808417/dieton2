<?php

namespace App\Http\Controllers\Admin;

use App\Meal;
use App\Sport;
use App\SportUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sports =  Sport::orderBy('created_at','DESC')->paginate(50);
        return view('admin.sports.index')->with('sports',$sports);
    }


    public function userSports($user_id){
            $sports =  SportUser::orderBy('created_at','DESC')->where('user_id',$user_id)->paginate(50);
            return view('admin.users.sports')->with('sports',$sports);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       return view('admin.sports.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(),[
            'name'=>'required',
            'sport_time'=>'required',
            'cals'=>'required',
            'image'=>'required|mimes:jpeg,jpg,bmp,png|max:2048',
        ]);


        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');
            return back();
        }

        $sport = new Sport();

        if ($request->has('image'))
        {
            $sport->image = uploader($request->image);
        }
        $sport->title = $request->name;
        $sport->default_sport_time = $request->sport_time;
        $sport->cals = $request->cals;
        $sport->created_at = Carbon::now();
        if($sport->save()){
            session()->flash('success','تم الحفظ  ');
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $sport = Sport::findOrFail($id);

        return view('admin.sports.edit')->with('sport',$sport);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validator = \Validator::make($request->all(),[
            'name'=>'required',
            'sport_time'=>'required',
            'cals'=>'required',
        ]);


        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');
            return back();
        }

        $sport = Sport::findOrFail($id);

        if ($request->has('image'))
        {
            $sport->image = uploader($request->image);
        }
        $sport->title = $request->name;
        $sport->default_sport_time = $request->sport_time;
        $sport->cals = $request->cals;
        $sport->created_at = Carbon::now();
        if($sport->save()){
            session()->flash('success','تم التعديل  ');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sport = Sport::findOrFail($id);

        if($sport->delete()){
            session()->flash('info','تم المسج  ');
        }

        return back();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Cart;
use App\CartProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CartsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $carts = Cart::where('status' ,'!=','in_cart')->paginate(50);

        return view('admin.carts.index')->with('carts',$carts);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($key)
    {
        //
        // get cart using key
        $carts = Cart::where('key',$key)->get();
        $order = CartProduct::where('key',$key)->first();
        return view('admin.carts.show')->with('carts',$carts)->with('order',$order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cart =  Cart::findOrFail($id);
        return view('admin.carts.edit')->with('cart',$cart);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active_order($id)
    {
        $cart =  CartProduct::findOrFail($id);

        if($cart){
            $cart->status = 'opening';
            $cart->save();
            session()->flash('success','تم التعديل  ');
            return redirect()->route('carts.index');
        }

    }

    public function refuse_order($id)
    {
        $cart =  CartProduct::findOrFail($id);

        if($cart){
            $cart->status = 'refused';
            $cart->save();
            session()->flash('success','تم الرفض  ');
            return redirect()->route('carts.index');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $cart = Cart::findOrFail($id);

        $validator = \Validator::make($request->all(),[
            'status'=>'required',
        ]);




        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');
            return back();
        }

        $cart->status = $request->status;


        if($cart->save()){
            session()->flash('success','تم التعديل  ');
            return back();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $CartProduct =  Cart::findOrFail($id);
        if($CartProduct->delete()){
            session()->flash('info','تم المسج  ');
        }

        return back();
    }
}

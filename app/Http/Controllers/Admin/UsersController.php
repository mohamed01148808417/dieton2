<?php

namespace App\Http\Controllers\Admin;

use App\CartProduct;
use App\Social;
use App\User;
use App\Water;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    //
    public function index(){
        $users = User::where('is_admin',null)->paginate(50);
         return view('admin.users.index')->with('users',$users);
    }

    public function login(){
        return view('admin.users.login');
    }
    public function doLogin(Request $request){
        $email = $request->get('email');
        $password = $request->get('password');

        // check is true user is admin
        if (Auth::attempt(['email' => $email, 'password' => $password]) && Auth::user()->is_admin == 1) {
            return view('admin.dashboard.index');
        }else{
            return redirect()->back()->with('error','you not admin');
        }
    }


    public function logout(Request $request) {
        Auth::logout();
        return redirect()->route('show_login');
    }

    public function create()
    {
        //
        return view('admin.users.create');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $validator = \Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'address'=>'nullable',
            'weight'=>'nullable',
            'height'=>'nullable',
            'age'=>'nullable',
            'default_cals'=>'nullable',
            'default_water'=>'nullable',
            'type'=>'nullable',
            'fitness'=>'nullable',
            'phone'=>'required|unique:users',
            'password' => 'required|confirmed|min:6',
            'image'=>'required|mimes:jpeg,jpg,bmp,png|max:2048'
        ]);

        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');

            return back();
        }

        $user = new User();

        if ($request->has(['password']))
        {
            $user->password = Hash::make($request->password);
        }

        if ($request->has('image'))
        {
            $user->image = uploader($request->image);
        }

        $user->name = $request->name;
        $user->address = $request->address;
        $user->weight = $request->weight;
        $user->height = $request->height;
        $user->age = $request->age;
        $user->default_cals = $request->default_cals;
        $user->default_water = $request->default_water;
        $user->type = $request->type;
        $user->fitness = $request->fitness;
        $user->phone= $request->phone;
        $user->email = $request->email;
        $user->active_message = $request->active_message;
        $user->created_at = Carbon::now();



        if($user->save()){

            session()->flash('success','تم الحفظ  ');

        }

        return back();


    }

    public function edit($id)
    {


        $user = User::findOrFail($id);
        return view('admin.users.edit')
            ->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user =  User::findOrFail($id);


        //dd($request->all());
        $validator = \Validator::make($request->all(),[
            'name'=>'required',
            'address'=>'nullable',
            'weight'=>'nullable',
            'height'=>'nullable',
            'age'=>'nullable',
            'default_cals'=>'nullable',
            'default_water'=>'nullable',
            'type'=>'nullable',
            'fitness'=>'nullable',
            'email'=>'required|email|unique:users,email,'.$user->id,
            'phone'=>'required|unique:users,phone,'.$user->id,
        ]);


        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');

            return back();
        }


        if ($request->has('image'))
        {
            $validator = \Validator::make($request->all(),[
                'image'=>'required|mimes:jpeg,jpg,bmp,png|max:2048'
            ]);

            if ($validator->fails())
            {
                session()->flash('error','خطأ في الصورة');

                return back();
            }

            $user->image = uploader($request->image);
        }



        $user->name = $request->name;
        $user->address = $request->address;
        $user->weight = $request->weight;
        $user->height = $request->height;
        $user->age = $request->age;
        $user->default_cals = $request->default_cals;
        $user->default_water = $request->default_water;
        $user->type = $request->type;
        $user->fitness = $request->fitness;
        $user->phone= $request->phone;
        $user->email = $request->email;
        $user->active_message = $request->active_message;
        $user->updated_at = Carbon::now();

        if($user->save()){

            session()->flash('success','تم التعديل  ');

        }

        return back();
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if($user->delete()){
            session()->flash('info','تم المسح  ');
            return redirect()->back();
        }
    }



    public function userCarts($user_id){



        $carts =  CartProduct::

            join('carts','carts.id','=','cart_product.cart_id')
            ->where('carts.user_id',$user_id)->paginate(50);

        return view('admin.users.carts')->with('carts',$carts);

    }


    public function userSocials($user_id){


        $socials =  Social::where('user_id',$user_id)->orderBy('created_at','DESC')->paginate(50);

        return view('admin.users.socials')->with('socials',$socials);

    }

    public function userSports($user_id){


        $socials =  Social::where('user_id',$user_id)->orderBy('created_at','DESC')->paginate(50);

        return view('admin.users.socials')->with('socials',$socials);

    }


    public function waters(){
        $waters = Water::paginate(50);

        return view('admin.users.waters')->with('waters',$waters);
    }


}

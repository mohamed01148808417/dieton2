<?php

namespace App\Http\Controllers\Admin;

use App\Meal;
use App\City;
use App\SportUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cities =  City::orderBy('created_at','DESC')->paginate(50);
        return view('admin.cities.index')->with('cities',$cities);
    }


   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       return view('admin.cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(),[
            'name'=>'required',
        ]);


        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');
            return back();
        }

        $city = new City();


        $city->name = $request->name;
        $city->created_at = Carbon::now();
        if($city->save()){
            session()->flash('success','تم الحفظ  ');
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $city = City::findOrFail($id);

        return view('admin.cities.edit')->with('city',$city);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validator = \Validator::make($request->all(),[
            'name'=>'required',

        ]);


        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');
            return back();
        }

        $city = City::findOrFail($id);


        $city->name = $request->name;

        $city->created_at = Carbon::now();
        if($city->save()){
            session()->flash('success','تم التعديل  ');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $city = City::findOrFail($id);

        if($city->delete()){
            session()->flash('info','تم المسج  ');
        }

        return back();
    }
}

<?php

namespace App\Http\Controllers\Admin;
use App\City;
use App\Regions;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class RegionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $regions =  Regions::orderBy('created_at','DESC')->paginate(50);
        return view('admin.regions.index')->with('regions',$regions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $cities = City::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE)->pluck('name', 'id');
       return view('admin.regions.create')->with('cities',$cities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(),[
            'name'=>'required',
            'city_id'=>'required',
        ]);




        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');
            return back();
        }

        $region = new Regions();

        $region->name = $request->name;
        $region->city_id = $request->city_id;
        $region->created_at = Carbon::now();
        if($region->save()){
            session()->flash('success','تم الحفظ  ');
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $cities = City::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE)->pluck('name', 'id');
        $region = Regions::findOrFail($id);
        return view('admin.regions.edit')->with('region',$region)->with('cities',$cities);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validator = \Validator::make($request->all(),[
            'name'=>'required',
            'city_id'=>'required',
        ]);




        if ($validator->fails())
        {
            session()->flash('error','خطأ في البيانات');
            return back();
        }

        $region = Regions::findOrFail($id);


        $region->name = $request->name;
        $region->city_id = $request->city_id;
        $region->created_at = Carbon::now();

        if($region->save()){
            session()->flash('success','تم التعديل  ');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $region = Regions::findOrFail($id);
        if($region->delete()){
            session()->flash('info','تم المسج  ');
        }

        return back();
    }
}

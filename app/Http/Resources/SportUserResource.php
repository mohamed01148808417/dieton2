<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SportUserResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'sports'=>$this->collection->transform(function ($q){
                return [
                    'id'            =>$q->id,
                    'title'          =>$q->sport->title,
                    'image'         =>getImg($q->sport->image),
                    'default_time'         =>$q->sport->default_sport_time,
                    'spend_time'         =>$q->spend_time,
                ];
            })
        ];

        //return parent::toArray($request);
    }
}

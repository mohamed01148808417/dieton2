<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MealResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'meals'=>$this->collection->transform(function ($q){
                return [
                    'id'            =>$q->id,
                    'title'          =>$q->name,
                    'created_at'         =>date($q->created_at)
                ];
            })
        ];


        //return parent::toArray($request);
    }
}

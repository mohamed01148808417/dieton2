<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'products'=>$this->collection->transform(function ($q){
                return [
                    'id'                =>$q->id,
                    'meal_id'              =>$q->meal_id,
                    'meal_title'              =>$q->meal->name,
                    'title'              =>$q->title,
                    'price'              =>$q->price,
                    'cals'              =>$q->cals,
                    'image'              =>getImg($q->image),
                    'active'              =>$q->active,
                    'desc'              =>$q->desc,
                    'created_at'        =>date($q->created_at),
                ];
            })
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PackageResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'packages'=>$this->collection->transform(function ($q){
                return [
                    'id'                    =>$q->id,
                    'title'                 =>$q->title,
                    'price'                 =>$q->price,
                    'details'               =>$q->details,
                    'expired_date'          =>$q->expired_date,
                    'number_of_mail'        =>$q->number_of_meal,
                ];
            })
        ];

        //return parent::toArray($request);
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class WatersResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'waters'=>$this->collection->transform(function ($q){
                return [
                    'id'                            =>$q->id,
                    'user_id'                       =>$q->user->id,
                    'user_name'                     =>$q->user->name,
                    'default_cup_of_water'          =>$q->user->default_water,
                    'cup_of_water'                  =>$q->cup_of_water,
                    'created_at'                    =>date($q->created_at),
                ];
            })
        ];


        //return parent::toArray($request);
    }
}

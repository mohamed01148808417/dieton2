<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use JWTAuth;

class SocialResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'socials'=>$this->collection->transform(function ($q){
                return [
                    'id'                =>$q->id,
                    'user_name'         =>$q->user->name,
                    'user_image'         =>getImg($q->user->image),
                    'user_weight'         =>$q->user->weight,
                    'user_tall'         =>$q->user->height,
                    'user_id'           =>$q->user_id,
                    'title'             => $q->title,
                    'data'              =>getImg($q->data),
                    'type'              =>$q->type,
                    'likes'              =>$q->likes,
                    'loves'              =>$q->loves,
                    'is_like'           =>  (is_like($q->id,\JWTAuth::user()->id)),
                    'is_love'           =>  (is_love($q->id,\JWTAuth::user()->id)),
                    'created_at'        =>date($q->created_at),
                ];
            })
        ];


        //return parent::toArray($request);
    }
}

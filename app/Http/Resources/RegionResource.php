<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RegionResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'regions'=>$this->collection->transform(function ($q){
                return [
                    'id'                =>$q->id,
                    'name'             =>$q->name,
                ];
            })
        ];

        //return parent::toArray($request);
    }
}

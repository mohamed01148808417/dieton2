<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CartProductResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'cart_product'=>$this->collection->transform(function ($q){
                return [
                    'cart_id'            =>$q->id,
                    'user_id'          =>$q->user_id,
                    'quantity'          =>$q->quantity,
                    'status'          =>$q->status,
                    'user_name'          =>$q->name,
                    'product_title'          =>$q->title,
                    'product_price'          =>$q->price,
                    'product_cals'          =>$q->cals,
                    'product_image'          =>getImg($q->image),
                    'created_at'         =>date($q->created_at)
                ];
            })
        ];
    }
}

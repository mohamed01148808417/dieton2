<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SportResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'sports'=>$this->collection->transform(function ($q){
                return [
                    'id'                =>$q->id,
                    'title'             =>$q->title,
                    'cals'             =>$q->cals,
                    'image'             =>getImg($q->image),
                    'default_time'         =>$q->default_sport_time,
                ];
            })
        ];

        //return parent::toArray($request);
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PackageUserResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'packages'=>$this->collection->transform(function ($q){
                return [

                    'id'                    =>$q->id,
                    'user'                  =>$q->user,
                    'title'                 =>$q->package->title,
                    'price'                 =>$q->package->price,
                    'code'                  =>($q->pay_number_id)? $q->cartNumber : null,
                    'details'               =>$q->package->details,
                    'expired_date'          =>$q->package->expired_date,
                    'number_of_meals'       =>$q->package->number_of_meal,
                    'current_number_of_meals'=>$q->current_number_of_meals,
                ];
            })
        ];

        //return parent::toArray($request);
    }
}

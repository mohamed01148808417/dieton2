<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'users'=>$this->collection->transform(function ($q){
                return [
                    'id'            =>$q->id,
                    'name'          =>$q->name,
                    'phone'         =>$q->phone,
                    'image'         =>getImg($q->image),
                    'email'         =>$q->email,
                    'active'        =>$q->active,
                    'active_message' =>$q->active_message,

                ];
            })
          ];
        //return parent::toArray($request);
    }
}

<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CartResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'cart'=>$this->collection->transform(function ($q){
                return [
                    'id'            =>$q->id,
                    'user_id'          =>$q->user_id,
                    'user_name'              =>$q->user->name,
                    'quantity'          =>$q->quantity,
                    'key'          =>$q->key,
                    'product_title'          =>$q->product->title,
                    'product_price'          =>$q->product->price,
                    'product_cals'          =>$q->product->cals,
                    'product_image'          =>getImg($q->product->image),
                    'created_at'         =>date($q->created_at)
                ];
            })
        ];
    }
}

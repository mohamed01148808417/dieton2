<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Water extends Model
{
    //

    protected $fillable = [
        'id',
        'user_id',
        'cup_of_water',
        'created_at'
    ];

    public function user(){

        return $this->belongsTo('App\User','user_id','id');

    }
}

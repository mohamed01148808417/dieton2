<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageUser extends Model
{
    //
    protected $fillable = ['user_id','package_id','pay_number_id','number_of_meals','current_number_of_meals','status'];

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function package(){
        return $this->belongsTo('App\Package','package_id','id');
    }

    public function cartNumber(){
        return $this->belongsTo('App\PayNumber','pay_number_id','id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //
    protected $fillable = ['id','title','details','price','number_of_meal','expired_date'];
}

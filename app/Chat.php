<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    //
    protected $fillable = [
        'sender',
        'receiver',
        'message',
        'has_file',
    ];


    protected $table = 'chats';

    public function senderChat(){

        return $this->belongsTo('App\User','sender','id');
    }

    public function receiverChat(){

        return $this->belongsTo('App\User','receiver','id');
    }
}

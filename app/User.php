<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','phone', 'password','image','active','active_message','city_id','region_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }


    public function socials(){
        return $this->hasMany('App\Social','user_id','id');
    }

    public function sports()
    {
        return $this->belongsToMany(Sport::class, 'today_sports');
    }

    public function Carts(){
        return $this->hasMany(Cart::class);
    }

    public function senderChats(){
        return $this->hasMany('App\Chat','sender','id');
    }

    public function receiverChats(){
        return $this->hasMany('App\Chat','receiver','id');
    }

    public function waters(){
        return $this->hasMany('App\Water','user_id','id');
    }

    public function city(){
        return $this->belongsTo('App\City','city_id','id');
    }

    public function region(){
        return $this->belongsTo('App\Regions','region_id','id');
    }

}
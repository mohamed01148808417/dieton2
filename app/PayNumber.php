<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayNumber extends Model
{
    //
    protected $fillable = [
        'id',
        'generated_number',
        'price',
        'created_at'
    ];

}

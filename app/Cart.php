<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $fillable = [
        'id',
        'user_id',
        'product_id',
        'quantity',
        'status',
        'total',
        'created_at',
    ];



    protected $table = 'carts';


    public function product(){
        return $this->belongsTo(Product::class);    }
    public function user(){
        return $this->belongsTo(User::class);
    }




}

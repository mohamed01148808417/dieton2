<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    //
    protected $fillable = ['id','user_id','social_id','type_of_like','created_at'];

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }


    public function social(){
        return $this->belongsTo('App\Social','social_id','id');
    }

    
}

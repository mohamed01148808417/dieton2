<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sport extends Model
{
    //
    protected $fillable = [
        'title',
        'image',
        'default_sport_time',
    ];
    public function users()
    {
        return $this->belongsToMany(User::class, 'sport_user');

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SportUser extends Model
{
    //

    protected $table = 'sport_user';
    protected $fillable = [
        'user_id',
        'sport_id',
        'spend_time'
    ];

    public function sport(){
        return $this->belongsTo('App\Sport','sport_id','id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $fillable = ['id','name','created_at','update_at'];

    public function regions(){
        return $this->hasMany('App\Regions','city_id','id');
    }
}

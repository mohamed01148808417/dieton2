<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regions extends Model
{
    //
    protected $fillable = ['id','city_id','name','created_at','update_at'];

    public function city(){
        return $this->belongsTo('App\City','city_id','id');
    }
}

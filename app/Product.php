<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable =[
        'title',
        'meal_id',
        'desc',
        'price',
        'image',
        'cals',
        'active',
    ];
    protected $table = 'products';

    public function meal(){
        return $this->belongsTo('App\Meal','meal_id','id');
    }

    public function carts(){
        return $this->belongsToMany(Cart::class);
    }
}

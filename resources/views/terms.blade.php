<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Terms and Conditions | Golf-gate</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="keywords" content="Quick Register Form Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design">
    <link href="{{asset('css/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('css/css/style.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('css/css/css.css')}}" rel="stylesheet">
    <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/droid-arabic-kufi" type="text/css"/>
    <style>
        p {
            font-family: 'DroidArabicKufiRegular';
            font-weight: normal;
            font-style: normal;
            direction: ltr;
            text-align: left;
            font-size: 16px;
            line-height: 30px;
            color: #000;
            letter-spacing: normal;
        }
    </style>
    <script language="javascript">

        function backtotop() {
            window.scrollTo(0,0);
        }

        function showSection (sectionId) {
            //document.location.href = "#"+sectionId;
            //showPolicySection (sectionId);
        }

        function showPolicySection (sectionId) {
            $('.policysection').hide();

            $("#backNav").show();
            if (sectionId=="landingPage") {
                $("#backNav").hide();
            }
            $('#'+sectionId).show();
        }
    </script><script language="javascript">

        $(document).ready(function(){
            //var sectionId = "landingPage";
            //showPolicySection(sectionId);
        });

        /* jQuery(window).bind('hashchange', function() {
             var sectionId = document.location.hash ? document.location.hash.substr(1) : "landingPage";
             showPolicySection(sectionId);
         });*/

    </script>
    <link id="avast_os_ext_custom_font" href="chrome-extension://eofcbnmajmjmplflapaojjnihcjkigck/common/ui/fonts/fonts.css" rel="stylesheet" type="text/css"></head>
<body>
<div class="signupform">
    <h1>  </h1>
    <div class="container">

        <div class="agile_info">
            <div class="w3_info" style="flex-basis: 99%; -webkit-flex-basis: 99%;">
                <h2 style="font-family: &#39;DroidArabicKufiRegular&#39;;
   font-weight: bold;
   font-style: normal;
   direction: ltr;
   text-align: center;
    font-size: 30px;
    line-height: 30px;
    letter-spacing: normal;
	display: block;
">Dieton<br/>
                    Terms and Conditions</h2>

                <h2 style="font-family: &#39;DroidArabicKufiRegular&#39;;
               font-weight: bold;
               font-style: normal;
               direction: ltr;
               text-align: center;
                font-size: 30px;
                line-height: 30px;
                letter-spacing: normal;
                display: block;
">
                    AGREEMENT FOR THE DietonWEBSITE AND SERVICES
                    <br/>
                    (THE "AGREEMENT")
                </h2>
                <div class="maia-article" role="article">
                    <div class="maia-teleport" id="content"></div>

                    <p>IMPORTANT: Carefully read this Agreement before using any Services.</p>

                    <p>This Agreement creates a binding legal agreement between you and DietonMobile Technology Ltd., and its suppliers and its licensors (collectively "we", "our" or "us").</p>

                    <p>BY USING THIS WEBSITE, AND ANY SERVICES OR SOFTWARE ON THE WEBSITE (THE "SERVICES"), YOU IRREVOCABLY ACCEPT THE TERMS AND CONDITIONS OF THIS AGREEMENT.
                        You also agree to ensure that anyone who uses the Services or software using your password or login information abides by this Agreement.</p>

                    <p>We have the right to revise any part of this Agreement at any time without providing notice to you.
                        Your continued use of the Services shall be deemed irrevocable acceptance of those revisions.</p>

                    <p>The Services provided to you under this Agreement are free, and only for your personal and non-commercial use.</p>

                    <h2>Description of the DietonSERVICES</h2>

                    <p>We will provide you access and use of the Services that enables you to view your insurance coverages and limits and send requests for changes to such coverages and limits to us.</p>

                    <p>We are not responsible to provide or maintain any hardware or other software required to use the Services.</p>

                    <h2>Right to Terminate or Modify Agreement and the Services</h2>

                    <p>This Agreement and your rights to use the Services shall terminate at any time that we decide, in our sole discretion, without notice to you.  This does not limit any of our other rights to terminate under this Agreement.</p>

                    <p>At any time, we may revise any part of this Agreement without providing notice to you and your continued use of the Services will be deemed irrevocable acceptance of those revisions.</p>

                    <p>You may terminate your use of the Services by simply ceasing to use the Services if we change this Agreement.</p>

                    <p>At any time and without cost, charge or liability, we may terminate your use or right to use the Services for any reason, including, but not limited to, a failure by you to comply with this Agreement.</p>

                    <p>At any time and without cost, charge or liability, we may modify or discontinue, temporarily or permanently, the Services (or any part thereof) with or without notice to you.</p>

                    <h2>Registration &amp; Personal Information</h2>

                    <p>For our providing you the Services, you must provide and maintain true, accurate, current and complete information about you as prompted by the Web Site registration process (an "Account").
                        As part of the registration process, each user will provide necessary information such as your name and will select a secure password ("Password").
                        You shall provide us with accurate, complete, and updated account information. Failure to do so shall constitute a breach of this Agreement, which may result in immediate termination of your Account. You may not (i) select or use a name of another person with the intent to impersonate that person; (ii) use a name subject to the rights of any other person without authorization;
                        or (iii) use a name that we, in our sole discretion, deem inappropriate or offensive.</p>

                    <p>You must have an Account to use the Services.</p>

                    <p>Personal information you provide to us is governed by our Privacy Policy which is incorporated by reference in to this Agreement, and laws related to personal information applicable to us.
                        Your use of the Services indicates your acceptance of the terms of the Privacy Policy.</p>

                    <p>You are responsible for maintaining the confidentiality of your Account information (including your Password) and are responsible for all activities that occur in your Account.  You will notify us immediately of any known or suspected unauthorized use(s) of your Account, or any known or suspected breach of security, including loss, theft, or unauthorized disclosure of your Password.</p>

                    <h2>Use of the DietonServices</h2>

                    <p>You agree that we may establish practices and procedures for any use of the Services.  We may, for example, set a maximum number of times and the duration that you can access the Services in a given time period.
                        We may modify these practices and procedures at any time, in our sole discretion, with or without notice.</p>

                    <p>All information that you send, save, collect or distribute through the Services, however transmitted, (the "Content") is your sole responsibility.
                        This means that you, and not us, are entirely responsible for all Content.</p>

                    <p>WE DO NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE, THAT DEFECTS WILL BE CORRECTED OR THAT THE SERVICES OR THE SERVER THAT MAKES THE SERVICES AVAILABLE WILL BE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.</p>

                    <p>We do not monitor or control the Content.
                        For clarity, Content does not include technical information regarding usage, data traffic, logs or other similar information that we may monitor.</p>

                    <p>WE DO NOT GUARANTEE THE USE, ACCURACY, INTEGRITY OR QUALITY OF ANY CONTENT DERIVED FROM THE USE OF THE SERVICES, OR EVEN THAT THE SERVICES WILL BE AVAILABLE TO YOU AT ANY TIME.</p>

                    <p>You will not use the Services to send, save, collect, distribute or transmit any materials, information or content that:</p>

                    <ol type="a">
                        <li>is harmful, threatening, abusive, harassing, tortious, defamatory, vulgar, obscene, libellous, invasive of another's privacy, hateful, or racially, ethnically or otherwise objectionable;</li>
                        <li>may manipulate identifiers in order to disguise the origin of any Content transmitted through the Services or the source of any Content;</li>
                        <li>you do not have a right to make available under any law or under contractual or fiduciary relationships;</li>
                        <li>downloads, transmits or otherwise makes available any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware;</li>
                        <li>interferes with or disrupts the Services or servers or networks connected to the Services, or disobey any requirements, procedures, policies or regulations of our networks connected to the Services; or</li>
                        <li>may violate or infringe any right of any third party.</li>
                    </ol>

                    <p>Any fraudulent, abusive, or otherwise illegal activity is grounds for termination of your Account and the use of the Services.</p>

                    <h2>Fees and Payments</h2>

                    <p>We reserve the right at any time to charge fees for access to portions of the Services or the Services as a whole.  However, in no event will you be charged for access to the Services unless we obtain your prior agreement to pay such charges.</p>

                    <p>If you elect not to pay any fees charged by us, we will cease providing the Services to you and this Agreement will automatically terminate.</p>

                    <h2>Title &amp; License</h2>

                    <p>All rights, title, interest, ownership, and intellectual property rights in and to the Services are, and will remain, ours or, as the case may be, our suppliers or licensors.  The software provided by us to use the Services are owned by, or licensed to, us and is protected by the laws of Canada and other countries, and by international treaties.
                        We make no claim to the right, title or interest in and to the Content provided by you.</p>

                    <p>You acknowledge that any ideas, suggestions, concepts, processes or techniques which you provide to us related to the Services ("Feedback") shall become our property without any compensation or other consideration payable to you by us, and you do so of your own free will and volition.  We may or may not, in our sole discretion, use or incorporate the Feedback in whatever form or derivative we may decide into the Services (or future versions or derivatives), our business or other products.
                        You hereby assign all rights to us in any Feedback and, as applicable, waive any moral rights.</p>

                    <p>You hereby grant to us a non-exclusive, non-transferrable, royalty-free, fully paid-up, worldwide license to copy, use, reproduce, modify, access, collect, store and use (i) the Content for the purpose of providing the Services to you Customer (and related activities associated with providing or improving the Services) during the Term;
                        and (ii) specifically, but without limiting the foregoing, any image or photograph that forms part of the Content may be used by us for the purposes of addressing, resolving, investigating, litigating, or otherwise relating to an application or renewal of insurance coverage,  or an insurance claim made by you or another insured for coverage under a policy provided by or through us, including a claim for insurance coverage relative to a third party claim, and we may provide such photograph or image to an insurer, adjuster, counsel, or other proper person for their exercise of the same rights for the same purposes described in this subsection (ii).</p>

                    <h2>Trademark</h2>

                    <p>Dieton and the <img style="width: 100px;" src="{{asset('css/img/logo.jpeg')}}"> logos are trademarks of Golf-gate.  All rights in them are reserved to Golf-gate.</p>

                    <h2>Disclaimer of Warranties</h2>

                    <p>THE SERVICES ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS.
                        YOU EXPRESSLY UNDERSTAND AND AGREE THAT THE USE OF THE SERVICES ARE AT YOUR SOLE RISK. </p>

                    <p>WE EXPRESSLY DISCLAIM, AND YOU HEREBY WAIVE, ALL WARRANTIES, REPRESENTATIONS AND CONDITIONS OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
                        WE MAKE NO WARRANTY THAT THE SERVICES WILL MEET YOUR REQUIREMENTS, BE UNINTERRUPTED, TIMELY, SECURE, VIRUS FREE OR ERROR FREE, OR THAT ANY ERRORS IN ANY SOFTWARE WE PROVIDE OR THE SERVICES WILL BE CORRECTED.</p>

                    <p>ANY CONTENT SENT, TRANSMITTED OR OBTAINED THROUGH THE USE OF THE SERVICES IS DONE AT YOUR OWN DISCRETION AND RISK.
                        YOU ARE SOLELY RESPONSIBLE FOR ANY DAMAGE TO ANY COMPUTER SYSTEM, MOBILE DEVICE OR DATA THAT RESULTS FROM THE USE OF THE SERVICES.</p>

                    <p>NO ADVICE OR INFORMATION, WHETHER WRITTEN OR ORAL, OBTAINED BY YOU FROM US, OUR EMPLOYEES, OFFICERS, DIRECTORS, AGENTS OR REPRESENTATIVES SHALL CREATE ANY WARRANTY OR CONDITION NOT EXPRESSLY STATED IN THIS AGREEMENT.</p>

                    <h2>Limitation of Liability</h2>

                    <p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT ARE WE OR OUR SUPPLIERS AND LICENSORS LIABLE FOR ANY DIRECT, CONSEQUENTIAL, INCIDENTAL, INDIRECT, SPECIAL, PUNITIVE, OR OTHER DAMAGES WHATSOEVER OR FOR DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, COMPUTER FAILURE, LOSS OF BUSINESS INFORMATION, OR OTHER LOSS ARISING OUT OF THE USE OF THE SERVICES, THE USE OR INABILITY TO USE THE SERVICES, THE MODIFICATION, SUSPENSION OR DISCONTINUATION OF THE SERVICES, YOUR ACCOUNT INFORMATION OR ANY CONTENT SENT, RECEIVED, STORED OR DISTRIBUTED VIA THE SERVICES, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</p>

                    <p>YOUR SOLE AND EXCLUSIVE REMEDY FOR ANY CLAIM, DAMAGE OR LOSS RELATED TO THE SERVICES SHALL BE CANCELLATION OF THE USE OF THE SERVICES.  IF A COURT AWARDS ANY DAMAGES AGAINST US, AND SUPPLIERS OR LICENSORS DESPITE THE FOREGOING LIMITATIONS OF LIABILITY IN THIS AGREEMENT, SUCH DAMAGES FOR ANY AND ALL CLAIMS UNDER THIS AGREEMENT, IN ANY CIRCUMSTANCE, SHALL ONLY BE FOR DIRECT DAMAGES AND NOT IN THEIR AGGREGATE EXCEED THE AMOUNT YOU PAID FOR THE USE OF THE SERVICES IN THE PRIOR SIX MONTHS TO WHEN THE CLAIM FIRST AROSE.</p>

                    <p>THE LIMITATIONS OF LIABILITY IN THIS SECTION SHALL APPLY WHETHER OR NOT THE ALLEGED BREACH OR DEFAULT IS A BREACH OF A FUNDAMENTAL CONDITION OR TERM, OR A FUNDAMENTAL BREACH.</p>

                    <p>YOU HEREBY WAIVE ANY RIGHT TO PARTICIPATE IN ANY CLASS ACTION OR HAVE A TRIAL BY JURY FOR ANY MATTER, DISPUTE, PROCEEDING OR ACTION ARISING OUT OF, OR RELATED TO, THIS AGREEMENT.</p>

                    <p>Because some jurisdictions do not allow the exclusion or limitation of liability for consequential or incidental damages, the above limitation may not apply to you</p>

                    <h2>Indemnification</h2>

                    <p>YOU AGREE TO INDEMNIFY AND HOLD HARMLESS US, OUR OFFICERS, DIRECTORS, EMPLOYEES, SUPPLIERS, LICENSORS AND AFFILIATES, FROM AND AGAINST ANY CLAIMS, LOSSES, DAMAGES, FINES AND EXPENSES (INCLUDING ATTORNEY'S FEES AND COSTS) ARISING OUT OF OR RELATING TO ANY CLAIMS THAT YOU HAVE USED THE SERVICES IN VIOLATION OF ANOTHER PARTY'S RIGHTS, IN VIOLATION OF ANY LAW, IN VIOLATION OF ANY TERMS OF THIS AGREEMENT, OR ANY OTHER CLAIM RELATED TO YOUR USE OF THE SERVICES.</p>

                    <h2>Termination</h2>

                    <p>We may terminate your use of the Services and access to the Services for any reason or for no reason, including but not limited to, your failure to comply with this Agreement or our Privacy Policy, a request by you to terminate your Account, discontinuance or material modification to the Services, unexpected technical issues or problems, extended periods of inactivity or requests by law enforcement or other government agencies. Upon termination, we will eliminate access to the Services and delete your account information and information stored within the Services, as permitted or required by law.</p>

                    <h2>Miscellaneous</h2>

                    <p>The use of the Services are personal to you, and the right to use the Services may not be transferred or assigned by you in part or in whole.</p>

                    <p>This Agreement has been made in and shall be construed and enforced in accordance with the laws of the jurisdiction of the Province of Alberta, Canada, and you irrevocably consent to submit to the exclusive jurisdiction of the courts of the Province of Alberta, Canada,  for any claim, proceeding or action under this Agreement.</p>

                    <p>The application of the United Nations Convention of Contracts for the International Sale of Goods is expressly excluded from applying to this Agreement.</p>

                    <p>You agree that this Agreement are specifically enforceable by injunctive relief and other equitable remedies without proof of monetary damages, in any jurisdiction.</p>

                    <p>This Agreement shall constitute the complete and exclusive agreement between you and us, and govern your use of the Services, and replaces and supersedes any other agreement, representation or discussion, oral or written, and may not be changed except in writing signed by us, regardless of whether or not the parties act under an unsigned "electronic" agreement or rely on such an unsigned agreement.</p>

                    <p>If any provision of this Agreement is declared by a court of competent jurisdiction to be invalid, illegal, or unenforceable, such provision shall be severed from this Agreement and the other provisions shall remain in full force and effect.</p>

                    <p>We may assign this Agreement to any third party in our sole discretion.</p>

                    <p>This Agreement binds you, your heirs, legal representatives, executors and permitted assigns.</p>

                    <p>The sections of "Title", "Disclaimer of Warranties", "Limitation of Liability", "Indemnification" and "Miscellaneous" will survive any actual or purported termination or expiry of this Agreement and continue in full force and effect.</p>

                    <p>You represent and warrant that you are authorized and permitted under all laws applicable to you: to the use the Services, to enter into this Agreement and comply with its terms, to meet your obligations hereunder and comply with all laws, regulations or policies that apply to the Services, including, without limitation, any and all import and export control regulations and laws.</p>

                    <h2 align="center">END OF AGREEMENT</h2>

                </div>
                <p></p>

                <div class="clear"></div>

            </div>
            <div class="w3l_form" style="flex-basis: 1%; -webkit-flex-basis: 1%;">

            </div>
            <div class="clear"></div>
        </div>

    </div>
    <div class="footer">

        <p style="direction: ltr;">© 2019 All Rights Reserved | Design by <a href="https://www.fekra-sa.com/" target="blank">الفكرة الرائعة لتقنية المعلومات</a></p>
    </div>
</div>

</body>
</html>
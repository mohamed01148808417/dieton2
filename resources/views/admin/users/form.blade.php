@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group col-md-12 pull-left">
    <label>الاسم </label>
    {!! Form::text("name",isset($user)? $user->name : null,['class'=>'form-control ','placeholder'=>'اكتب الاسم هنا'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>البريد الالكتورني </label>
    {!! Form::text("email",isset($user)? $user->email : null,['class'=>'form-control ','placeholder'=>'اكتب البريد الالكتروني هنا'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>العنوان </label>
    {!! Form::text("address",isset($user)? $user->address : null,['class'=>'form-control ','placeholder'=>'اكتب العنوان هنا'])!!}
</div>


<div class="form-group col-md-12 pull-left">
    <label>رقم الجوال </label>
    {!! Form::text("phone",isset($user)? $user->phone : null,['class'=>'form-control ','placeholder'=>'اكتب رقم الجوال هنا'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>الوزن </label>
    {!! Form::text("weight",isset($user)? $user->weight : null,['class'=>'form-control ','placeholder'=>'اكتب الوزن هنا'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>الطول </label>
    {!! Form::text("height",isset($user)? $user->height : null,['class'=>'form-control ','placeholder'=>'اكتب الطول هنا'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>العمر </label>
    {!! Form::text("age",isset($user)? $user->age : null,['class'=>'form-control ','placeholder'=>'اكتب العمر هنا'])!!}
</div>


<div class="form-group col-md-12 pull-left">
    <label>عدد الكالوري </label>
    {!! Form::text("default_cals",isset($user)? $user->default_cals : null,['class'=>'form-control ','placeholder'=>'اكتب عدد الكالوري هنا'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>عدد اكواب المياة</label>
    {!! Form::text("default_water",isset($user)? $user->default_water : null,['class'=>'form-control ','placeholder'=>'اكتب عدد اكواب المياة هنا'])!!}
</div>
<div class="form-group">
    <label>    النوع</label>
    {{ Form::select('type', [
        'male'  => 'ذكر',
        'female'  => 'انثي'
    ], isset($user)? $user->type : null,array('class'=>'form-control', 'placeholder'=>'اختار نوع هنا ')) }}
</div>


<div class="form-group">
    <label>    اللياقة</label>
    {{ Form::select('fitness', [
        'weak'  => 'ضعيف',
        'good'  => 'جيد',
        'very_good'  => 'جيد جدا',
    ],isset($user)? $user->fitness : null, array('class'=>'form-control', 'placeholder'=>'اختار اللياقة هنا ')) }}
</div>


<div class="form-group">
    <label>    تفعيل الشات للمستخدم</label>
    {{ Form::select('active_message', [
        '0'  => 'الغاء التقعيل',
        '1'  => 'تفعيل الشات',
    ],isset($user)? $user->active_message : null, array('class'=>'form-control', 'placeholder'=>'اختر حالة التفعيل هنا ')) }}
</div>
@if(!isset($user))
    <div class="form-group col-md-12 pull-left">
        <label>الرقم السري </label>
        {!! Form::input("password",'password',null,['class'=>'form-control ','placeholder'=>'اكتب الرقم السري هنا'])!!}
    </div>

    <div class="form-group col-md-12 pull-left">
        <label>اعد كتابه الرقم السري </label>
        {!! Form::input("password",'password_confirmation',null,['class'=>'form-control ',
        'placeholder'=>'اعد كتابه الرقم السري هنا'])!!}
    </div>
@endif
<div class="form-group col-md-12 pull-left">
    <label>صورة المستخدم </label>
    {!! Form::file("image",null,['class'=>'form-control ','required'])!!}
    <br>
    <br>
    <br>
    <div class="clearfix"></div>
    @if(isset($user))
        @if($user->image)
            <label> صورة القديمة  </label>
        <div class="clearfix"></div>

        <img src="{{getImg($user->image)}}"/>
        @endif
    @endif

</div>


<br>
<br>
<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>

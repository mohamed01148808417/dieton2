@extends('admin.layouts.home')
@section('title')
    وسائل التواصل للمستخدم
@endsection

@section('content')


@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">    وسائل التواصل للمستخدم </h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            وسائل التواصل للمستخدم </div>
        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th>الاسم </th>
                <th>الصورة </th>
                <th>likes </th>
                <th>loves </th>
            </tr>
            </thead>
            <tbody>
            @foreach($socials as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->user->name}}</td>
                    <td><img src="{{getImg($item->data)}}" class="img-responsive" style="width: 200px; height: 200px"/> </td>
                    <td>{{$item->likes}}</td>
                    <td>{{$item->loves}}</td>
                    <td></td>
                </tr>
                <!-- Modal -->
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

    <div class="links">
        {{ $socials->links() }}
    </div>





    <div class="row">
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>

    </div>

@endsection



@extends('admin.layouts.home')
@section('title')
عربات المستخدم
@endsection

@section('content')


@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">كل المستخدم   </h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
كل العربات          </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th>عنوان </th>
                <th>اسم المستخدم </th>
                <th>الكمية </th>
                <th>الحالة </th>
                <th>سعر الوجبة </th>
                <th>صورة الوجبة </th>
                <th>العمليات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($carts as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->product->title}}</td>
                    <td>{{$item->cart->user->name}}</td>
                    <td>{{$item->cart->quantity}}</td>
                    <td>
                        @if($item->cart->status == 'pending' )
                            {{'pending'}}
                        @elseif($item->cart->status == 'opening' )
                            {{' opening '}}
                        @elseif($item->cart->status == 'refused' )
                            {{' refused '}}
                        @endif
                    </td>
                    <td>{{$item->product->price}}</td>
                    <td><img src="{{getImg($item->product->image)}}" class="img-responsive" style="width: 200px; height: 200px"/> </td>



                    {!!Form::open( ['route' => ['carts.destroy',$item->id] ,
                    'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                    {!!Form::close() !!}
                    <td>
                        <a href="{{route('carts.edit',
                        ['id'=>$item->cart->id])}}" data-toggle="tooltip"
                           data-original-title="تعديل">
                            <i class="icon-pencil7 text-inverse" style="margin-left: 10px"></i> </a>
                        <a href="#" onclick="Delete({{$item->id}})" data-toggle="tooltip" data-original-title="حذف">
                            <i class="icon-trash text-inverse text-danger" style="margin-left: 10px"></i> </a>
                    </td>

                </tr>
                <!-- Modal -->
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

    <div class="links">
        {{ $carts->links() }}
    </div>




    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف  ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف القسم تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }



    </script>

    <div class="row">
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>

    </div>

@endsection



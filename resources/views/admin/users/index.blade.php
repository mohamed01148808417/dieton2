@extends('admin.layouts.home')
@section('title')
المستخدمين
@endsection

@section('content')


@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">كل العضويات</h5>
            <div class="heading-elements">
                <ul class="icons-list">

                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">



            <br>
            <br>
            عرض كل العضويات والتحكم بهم وبكل العمليات الخاصة بهم مع امكانية البحث وتصدير تقارير وملفات وطباعتهم
        </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th>الاسم </th>
                <th>البريد</th>
                <th>الموبايل</th>
                <th>العمليات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->phone}}</td>
                    {!!Form::open( ['route' => ['users.destroy',$item->id] ,
                    'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                    {!!Form::close() !!}
                    <td>
                        <a href="{{route('userCarts',
                        ['id'=>$item->id])}}" data-toggle="tooltip"
                           data-original-title="عربات المستخدم ">
                            <i class="icon-cart text-inverse" style="margin-left: 10px"></i> </a>

                        <a href="{{route('userSports',
                        ['id'=>$item->id])}}" data-toggle="tooltip"
                           data-original-title="رياضات المستخدم ">
                            <i class="icon-file-spreadsheet text-inverse" style="margin-left: 10px"></i> </a>
                        <a href="{{route('userSocials',
                        ['id'=>$item->id])}}" data-toggle="tooltip"
                           data-original-title="وسائل التواصل ">
                            <i class="icon-image-compare text-inverse" style="margin-left: 10px"></i> </a>

                        <a href="{{route('users.edit',
                        ['id'=>$item->id])}}" data-toggle="tooltip"
                           data-original-title="تعديل">
                            <i class="icon-pencil7 text-inverse" style="margin-left: 10px"></i> </a>

                        <a href="#" onclick="Delete({{$item->id}})" data-toggle="tooltip" data-original-title="حذف">
                            <i class="icon-trash text-inverse text-danger" style="margin-left: 10px"></i> </a>
                    </td>

                </tr>
                <!-- Modal -->
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

    <div class="links">
        {{ $users->links() }}
    </div>




    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا المستخدم ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف القسم تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }



    </script>

    <div class="row">
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>

    </div>

@endsection



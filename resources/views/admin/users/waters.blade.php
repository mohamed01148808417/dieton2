@extends('admin.layouts.home')
@section('title')
اكواب المياة للمستخدمين
@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"> اكواب المياة للمستخدمين  </h5>
            <div class="heading-elements">
                <ul class="icons-list">

                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">اكواب المياة للمستخدمين</div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th>الاسم </th>
                <th>عدد اكواب المياة الافتراضية </th>
                <th>عدد اكواب المياة المشروبة </th>
                <th style="float: none">التاريخ </th>
            </tr>
            </thead>
            <tbody>
            @foreach($waters as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->user->name}}</td>
                    <td>{{$item->user->default_water}}</td>
                    <td>{{$item->cup_of_water}}</td>
                    <td style="float: none;">{{$item->created_at->format('Y-m-d')}}</td>
                </tr>
                <!-- Modal -->
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

    <div class="links">
        {{ $waters->links() }}
    </div>
    <div class="row">
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>

    </div>
@endsection



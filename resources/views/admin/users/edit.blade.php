@extends('admin.layouts.home')
@section('title')
اضافة عضو جديد
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">اضافة عضو جديد</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    {!!Form::model($user, ['route' => ['users.update' , $user->id] ,
                    'class'=>'form phone_validate', 'method' => 'PATCH','files' => true]) !!}
                        @include('admin.users.form')
                    {!!Form::close() !!}
                </div>
            </div>
        </div>
    </div>
<!-- #END# Basic Validation -->
@endsection

@section('script')

    <script type="text/javascript" src="{{asset('admin/admin/assets/js/pages/form_layouts.js')}}"></script>
@endsection

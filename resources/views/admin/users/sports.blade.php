@extends('admin.layouts.home')
@section('title')
الرياضيات
@endsection

@section('content')


@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"> رياضيات المستخدم  </h5>
            <div class="heading-elements">
                <ul class="icons-list">

                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            رياضيات المستخدم </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th>الاسم </th>
                <th>الوقت </th>
                <th>الوقت المستنفذ</th>
                <th>عدد الكالوي </th>
                <th>الصورة </th>

            </tr>
            </thead>
            <tbody>
            @foreach($sports as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->sport->title}}</td>
                    <td>{{$item->sport->default_sport_time}}</td>
                    <td>{{$item->spend_time}}</td>
                    <td>{{$item->sport->cals}}</td>
                    <td><img src="{{getImg($item->sport->image)}}" class="img-responsive" style="width: 200px; height: 200px"/> </td>
                </tr>
                <!-- Modal -->
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

    <div class="links">
        {{ $sports->links() }}
    </div>

    <div class="row">
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>

    </div>

@endsection



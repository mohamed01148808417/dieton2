@extends('admin.layouts.home')
@section('title')
طلب المستخدم
@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">طلب المستخدم  </h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
طلب المستخدم          </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th>اسم المستخدم </th>
                <th>اسم الوجبة </th>
                <th>صورة الوجبة </th>
                <th>الكمية </th>
                <th>سعر الوجبة </th>
            </tr>

            </thead>
            <tbody>
            @foreach($carts as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->user->name}}</td>
                    <td>{{$item->product->title}}</td>
                    <td><img src="{{getImg($item->product->image)}}" style="width: 200px;height: 200px; border-radius: 50px"></td>
                    <td class="text-center"><span style="background: red;

                     display: inline-block;
                     width: 40px;
                     height: 40px;
                     line-height: 40px;
                     border-radius: 50%;
                     color: #FFF;

                     ">{{$item->quantity}}</span></td>
                    <td style="float: none" class="text-center"><span style="background: red;

                     display: inline-block;
                     width: 40px;
                     height: 40px;
                     line-height: 40px;
                     border-radius: 50%;
                     color: #FFF;

                     ">{{$item->product->price}}</span></td>
                </tr>
                <!-- Modal -->
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

    <div>

        <h2> تفاصيل الطلب </h2>
        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th>رقم الطلب</th>
                <th>حالة الطلب</th>
                <th>السعر الكلي للطلب</th>
                <th>العمليات</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$order->key}}</td>
                <td>{{$order->status}}</td>
                <td>{{$order->total}}</td>
                <td style="float: none; text-align: center">
                    @if($order->status == 'pending')
                        <a style=" margin-top:5px;width: 150px; height: 40px; background-color: #0b76cc; border-radius: 0" class="btn btn-success" href="{{route('active_order',
                        ['id'=>$order->id])}}" data-toggle="tooltip"
                           data-original-title="تعديل">
                            تنشيط الطلب
                        </a>


                        <a style=" margin-top:5px;width: 150px; height: 40px; background-color: #9c1f1f; border-radius: 0" class="btn btn-danger"
                           href="{{route('refuse_order',
                        ['id'=>$order->id])}}" data-toggle="tooltip"
                           data-original-title="تعديل">
                            رفض الطلب
                        </a>

                    @endif




                </td>
            </tr>
            </tbody>
        </table>
    </div>




    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف  ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف القسم تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }



    </script>

    <div class="row">
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>

    </div>

@endsection



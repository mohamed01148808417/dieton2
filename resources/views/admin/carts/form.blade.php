@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group">
    <label>حالة الوجبة   </label>
    {{ Form::select('status', [
        'pending' => 'لم يتم الرد',
        'refused' => 'مرفوض',
        'opening' => 'تم التسليم',
    ],(isset($cart))?$cart->status : null , array('class'=>'form-control', 'placeholder'=>'اختار حالة العربة ')) }}
</div>


<br>
<br>
<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>

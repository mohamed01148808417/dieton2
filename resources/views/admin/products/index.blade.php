@extends('admin.layouts.home')
@section('title')
المنتجات
@endsection

@section('content')


@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">كل المنتجات </h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
كل الاقسام         </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th>عنوان </th>
                <th>نوع الوجبة </th>
                <th>سعر الوجبة </th>
                <th>عدد الكالوري </th>
                <th>حالة الوجبة </th>
                <th>صورة الوجبة </th>
                <th>العمليات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->title}}</td>
                    <td>{{$item->meal->name}}</td>
                    <td>{{$item->price}}</td>
                    <td>{{$item->cals}}</td>
                    <td>
                        @if($item->active == 1 )
                            {{'مفعل'}}
                        @else
                            {{'غير مفعل '}}
                        @endif
                    </td>

                    <td><img src="{{getImg($item->image)}}" class="img-responsive" style="width: 200px; height: 200px"/> </td>

                    {!!Form::open( ['route' => ['products.destroy',$item->id] ,
                    'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                    {!!Form::close() !!}
                    <td>
                        <a href="{{route('products.edit',
                        ['id'=>$item->id])}}" data-toggle="tooltip"
                           data-original-title="تعديل">
                            <i class="icon-pencil7 text-inverse" style="margin-left: 10px"></i> </a>
                        <a href="#" onclick="Delete({{$item->id}})" data-toggle="tooltip" data-original-title="حذف">
                            <i class="icon-trash text-inverse text-danger" style="margin-left: 10px"></i> </a>
                    </td>

                </tr>
                <!-- Modal -->
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

    <div class="links">
        {{ $products->links() }}
    </div>




    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف  ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف القسم تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }



    </script>

    <div class="row">
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>

    </div>

@endsection



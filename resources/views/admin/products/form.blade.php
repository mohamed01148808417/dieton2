@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group col-md-12 pull-left">
    <label>عنوان المنتج </label>
    {!! Form::text("title",(isset($product))?$product->title : null,['class'=>'form-control ','placeholder'=>'اكتب الاسم هنا'])!!}
</div>


<div class="form-group">
    <label>نوع المنتج  </label>
    {{ Form::select('meal_id', $meals,(isset($product))?$product->meal_id : null , array('class'=>'form-control', 'placeholder'=>'اختار نوع الوجبة ')) }}
</div>

<div class="form-group col-md-12 pull-left">
    <label>سعر المنتج </label>
    {!! Form::text("price",(isset($product))?$product->price : null,['class'=>'form-control ','placeholder'=>'اكتب السعر'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>دد الكالوري ف الوجبة </label>
    {!! Form::text("cals",(isset($product))?$product->cals : null,['class'=>'form-control ','placeholder'=>'اكتب عدد الكالوري'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>صورة الوجبة </label>
    {!! Form::file("image",null,['class'=>'form-control ','required'])!!}
    <br>
    <br>
    <br>
    <div class="clearfix"></div>
    @if(isset($product))
        @if($product->image)
            <label> صورة القديمة  </label>
            <div class="clearfix"></div>

            <img src="{{getImg($product->image)}}"/>
        @endif
    @endif

</div>



<div class="form-group col-md-12 pull-left">
    <label>تفاصيل المنتج </label>
    {!! Form::textarea("desc",(isset($product))?$product->desc : null,['class'=>'form-control ','placeholder'=>'اكتب تفاصيل المنتج '])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>حالة الوجبة   </label>
    {{ Form::select('active', [
        '0' => 'غير مفعل',
        '1' => 'مفعل',
    ],(isset($product))?$product->active : null , array('class'=>'form-control', 'placeholder'=>'اختار حالة الوجبة ')) }}
</div>


<br>
<br>
<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>

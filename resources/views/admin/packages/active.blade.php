@extends('admin.layouts.home')
@section('title')
الباقات المفعلة
@endsection

@section('content')


@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">الباقات المفعلة </h5>
            <div class="heading-elements">
                <ul class="icons-list">

                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            الباقات المفعلة        </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th>العنوان </th>
                <th>اسم المستخدم </th>
                <th>صورة الشيك </th>
                <th>عدد الوجبات الكلية </th>
                <th>عدد الوجبات المتبقية </th>
                <th>السعر </th>
                <th>المدة </th>
                <th>العمليات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($packages as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->package->title}}</td>
                    <td>{{$item->user->name}}</td>
                    <td><img width="200px" height="200px" src="{{getImg($item->image)}}"></td>
                    <td>{{$item->number_of_meals}}</td>
                    <td>{{$item->current_number_of_meals}}</td>
                    <td>{{$item->package->price}}</td>
                    <td>{{$item->package->expired_date}} يوم</td>
                    
                    {!!Form::open( ['route' => ['destroy_package',$item->id] ,
                    'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                    {!!Form::close() !!}

                    {!!Form::open( ['route' => ['un_active_item',$item->id] ,
                    'id'=>'unactive-form'.$item->id, 'method' => 'post']) !!}
                    {!!Form::close() !!}


                    <td>

                        <a href="#" onclick="unActive({{$item->id}})" data-toggle="tooltip"
                           data-original-title="حذف">
                            <i class="icon-add text-inverse text-danger"
                               style="margin-left: 10px"></i> </a>

                        <a href="#" onclick="Delete({{$item->id}})" data-toggle="tooltip" data-original-title="حذف">
                            <i class="icon-trash text-inverse text-danger" style="margin-left: 10px"></i> </a>
                    </td>

                </tr>
                <!-- Modal -->
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->

    <div class="links">
        {{ $packages->links() }}
    </div>




    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف  ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف القسم تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }

        function unActive(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد من الغاء التفعيل ",
                text: "هل تريد الغاء التفاعيل ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('unactive-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "مازالت الباقة مفعله",'info',{buttons:'موافق'});
                }
            });
        }



    </script>

    <div class="row">
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>

    </div>

@endsection



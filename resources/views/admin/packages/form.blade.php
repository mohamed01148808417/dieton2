@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group col-md-12 pull-left">
    <label>عنوان الباقة  </label>
    {!! Form::text("title",(isset($package))?$package->title : null,
    ['class'=>'form-control ','placeholder'=>'اكتب عنوان الباقة'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>عدد الوجبات داخل الباقة  </label>
    {!! Form::number("number_of_meal",(isset($package))?$package->number_of_meal : null,
    ['class'=>'form-control ','placeholder'=>'اكتب عدد الوجبات داخل الباقة'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>سعر الباقة  </label>
    {!! Form::number("price",(isset($package))?(integer)$package->price : null,
    ['class'=>'form-control ','placeholder'=>'اكتب سعر الباقة  '])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>مده الباقة  </label>
    {!! Form::number("expired_date",
    (isset($package))?(integer)$package->expired_date : null,
    ['class'=>'form-control ','placeholder'=>'اكتب مده الباقة'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>مده الباقة  </label>
    {!! Form::textarea("details",(isset($package))?$package->details : null,
    ['class'=>'form-control ','placeholder'=>'اكتب التفاصيل'])!!}
</div>

<br>
<br>
<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>

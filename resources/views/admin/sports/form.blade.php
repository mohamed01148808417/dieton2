@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group col-md-12 pull-left">
    <label>اسم الرياضة الرئيسي  </label>
    {!! Form::text("name",(isset($sport))?$sport->title : null,['class'=>'form-control ','placeholder'=>'اكتب الاسم هنا'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>وقت تنفيذ الرياضة  </label>
    {!! Form::time("sport_time",(isset($sport))?$sport->default_sport_time : \Carbon\Carbon::now() ,
    ['class'=>'form-control ','placeholder'=>'اكتب الاسم هنا'])!!}
</div>


<div class="form-group col-md-12 pull-left">
    <label>عدد الكالوري  </label>
    {!! Form::text("cals",(isset($sport))?$sport->cals : null ,
    ['class'=>'form-control ','placeholder'=>'اكتب عدد الكالوري هنا'])!!}
</div>

<div class="form-group col-md-12 pull-left">
    <label>صورة الرياضة </label>
    {!! Form::file("image",null,['class'=>'form-control ','required'])!!}
    <br>
    <br>
    <br>
    <div class="clearfix"></div>
    @if(isset($sport))
        @if($sport->image)
            <label> صورة القديمة  </label>
            <div class="clearfix"></div>

            <img src="{{getImg($sport->image)}}"/>
        @endif
    @endif

</div>

<br>
<br>
<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>

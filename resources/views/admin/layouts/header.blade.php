<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        لوحة تحكم موقع داستون
        |
        @yield('title')</title>
    <!-- Global stylesheets -->
    <!--    Favicon-->
    <!-- Favicon -->
    <meta name="msapplication-TileColor" content="#F7630C">
    <meta name="msapplication-TileImage" content="{{asset('website/assets/ico/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#F7630C">
    <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
    <link href="{{asset('admin/admin/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/admin/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/admin/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/admin/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/admin/assets/css/extras/animate.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/admin/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/admin/assets/css/customized.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
@yield('header')
@stack('header')
<!-- Core JS files -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript"
            src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyBQ9fjKJMbJHN-Xs8zEOIIk6CApqefwMgQ'></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
{{-- <link href="{{asset('admin/assets/icon-picker/css/fontawesome-iconpicker.min.css')}}" rel="stylesheet">--}}
<!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/pickers/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/pagination/bootpag.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/pagination/bs_pagination.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/pagination/datepaginator.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    {{--  @include('sweet::alert')--}}
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/plugins/ui/nicescroll.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/admin/assets/js/pages/components_pagination.js')}}"></script>
    {{--<script type="text/javascript" src="{{asset('admin/assets/js/pages/layout_fixed_custom.js')}}"></script>--}}
    @yield('script')

</head>

<body>

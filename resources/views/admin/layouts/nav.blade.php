
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        {{--<a class="navbar-brand" href="/dashboard"><img src="/admin/assets/images/logo.png" alt=""></a>--}}
        <a class="navbar-brand" href="{{url('/admin/dashboard')}}">
            {{--        <img src="{{asset('admin/assets/images/logo.png')}}" class="position-left" alt="">  --}}
        </a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

        </ul>

        {{--<p class="navbar-text"><span class="label bg-success">متصل اﻵن</span></p>--}}

        <ul class="nav navbar-nav navbar-right">

            {{--<li class="dropdown language-switch">--}}
            {{--<a class="dropdown-toggle" data-toggle="dropdown">--}}
            {{--<img src="{{asset('admin/assets/images/flags/sa.png')}}" class="position-left" alt="">--}}
            {{--عربي--}}
            {{--</a>--}}
            {{--</li>--}}

            {{--      <li class=" dropdown notifications-wrapper">--}}
            {{--        <a href="#" data-toggle="dropdown" class="btn btn-link btn-float has-text">--}}
            {{--          <i class="icon-earth text-primary"></i>--}}
            {{--          <span class="noti-badge">{{auth()->user()->unreadNotifications()->whereType('App\Notifications\CartNotification')->count()}}</span>--}}
            {{--        </a>--}}
            {{--        <ul class="dropdown-menu animated fadeInUp the-notifications-wrapper">--}}
            {{--            @foreach(auth()->user()->unreadNotifications()->whereType('App\Notifications\CartNotification')->orderBy('id','desc')->get() as $notification)--}}
            {{--          <li>--}}
            {{--            تم اضافه عربه جديده للمشاهده--}}
            {{--            <a href="{{route('admin.cart-items.show',$notification->data['cart_id'])}}" class="makeAsRead" data-id="{{$notification->id}}">--}}
            {{--              اضغط هنا--}}
            {{--            </a>--}}
            {{--          </li>--}}
            {{--                @endforeach--}}
            {{--            <div class="fetchReaded">--}}

            {{--            </div>--}}
            {{--                <li class="read-all-notis">--}}
            {{--                    <a href="javascript:" class="readedNotifications">مشاهده كل الاشعارات</a>--}}
            {{--                </li>--}}
            {{--        </ul>--}}
            {{--      </li>--}}

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <span>#</span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{route('do_logout')}}"><i class="icon-switch2"></i> تسجيل خروج</a></li>
                </ul>
            </li>




        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
        <div class="sidebar sidebar-main ">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left">
                                <img src="" class="img-circle img-sm" alt="">
                            </a>

                            <div class="media-body">
                                <span class="media-heading text-semibold">#</span>
                                <div class="text-size-mini text-muted">
                                    <i class="icon-pin text-size-small"></i> مدير الموقع
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /user menu -->

                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">


                        <ul class="navigation navigation-main navigation-accordion">


                            <!-- Main -->
                            <li class="navigation-header"><span>الاعدادات الرئيسية</span> <i class="icon-menu"
                                                                                             title="Main pages"></i></li>
                            <li class="{{(route('dashboard') ? 'active' : '')}}"><a href="{{route('dashboard')}}"><i
                                            class="icon-home4"></i> <span>الصفحة الرئيسية</span></a></li>

                            <li class="navigation-header"><span>المستخدمين والفروع </span> <i class="icon-menu" title="Main pages"></i></li>

                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>الاعضاء</span></a>
                                <ul>
                                    <li class="{{(Request::is('users.index') ? 'active' : '')}}"><a
                                                href="{{route('users.index')}}"><i class="icon-list"></i> كل الاعضاء</a>
                                    </li>
                                    <li class="{{(Request::is('users.create') ? 'active' : '')}}"><a
                                                href="{{route('users.create')}}"><i class="icon-add-to-list"></i> اضافة
                                            عضو جديد</a>
                                    </li>
                                </ul>
                            </li>


                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>اقسام الوجبات </span></a>
                                <ul>
                                    <li class="{{(Request::is('meals.index') ? 'active' : '')}}"><a
                                                href="{{route('meals.index')}}"><i class="icon-list"></i> كل الوجبات</a>
                                    </li>
                                    <li class="{{(Request::is('meals.create') ? 'active' : '')}}"><a
                                                href="{{route('meals.create')}}"><i class="icon-add-to-list"></i> اضافة
                                           وجبة</a>
                                    </li>
                                </ul>
                            </li>


                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>كروت الشحن </span></a>
                                <ul>
                                    <li class="{{(Request::is('payNumbers.index') ? 'active' : '')}}"><a
                                                href="{{route('payNumbers.index')}}"><i class="icon-list"></i> كل كروت الشحن </a>
                                    </li>
                                    <li class="{{(Request::is('payNumbers.create') ? 'active' : '')}}"><a
                                                href="{{route('payNumbers.create')}}"><i class="icon-add-to-list"></i> اضافة
                                            كارت شحن </a>
                                    </li>
                                </ul>
                            </li>

                          <!--
                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>وسائل التواصل  </span></a>
                                <ul>
                                    <li class="{{(Request::is('socials.index') ? 'active' : '')}}"><a
                                                href="{{route('socials.index')}}"><i class="icon-list"></i> كل وسائل التواصل</a>
                                    </li>
                                </ul>
                            </li>
-->
                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>اقسام الرياضيات </span></a>
                                <ul>
                                    <li class="{{(Request::is('sports.index') ? 'active' : '')}}"><a
                                                href="{{route('sports.index')}}"><i class="icon-list"></i> كل الرياضيات</a>
                                    </li>
                                    <li class="{{(Request::is('sports.create') ? 'active' : '')}}"><a
                                                href="{{route('sports.create')}}"><i class="icon-add-to-list"></i> اضافة
                                            رياضة</a>
                                    </li>
                                </ul>
                            </li>


                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>الباقات  </span></a>
                                <ul>
                                    <li class="{{(Request::is('packages.index') ? 'active' : '')}}"><a
                                                href="{{route('packages.index')}}"><i class="icon-list"></i> كل الباقات</a>
                                    </li>


                                    <li class="{{(Request::is('active') ? 'active' : '')}}"><a
                                                href="{{route('active')}}">
                                            <i class="icon-list"></i>  الباقات المفعلة</a>
                                    </li>


                                    <li class="{{(Request::is('active_item') ? 'active' : '')}}"><a
                                                href="{{route('un_active')}}">
                                            <i class="icon-list"></i>  الباقات الغير مفعلة</a>
                                    </li>


                                    <li class="{{(Request::is('packages.create') ? 'active' : '')}}"><a
                                                href="{{route('packages.create')}}"><i class="icon-add-to-list"></i> اضافة
                                            باقة</a>
                                    </li>
                                </ul>
                            </li>



                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>المنتجات</span></a>
                                <ul>
                                    <li class="{{(Request::is('products.index') ? 'active' : '')}}"><a
                                                href="{{route('products.index')}}"><i class="icon-list"></i> كل المنتجات</a>
                                    </li>
                                    <li class="{{(Request::is('products.create') ? 'active' : '')}}"><a
                                                href="{{route('products.create')}}"><i class="icon-add-to-list"></i> اضافة
                                            منتج</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>المدن</span></a>
                                <ul>
                                    <li class="{{(Request::is('cities.index') ? 'active' : '')}}"><a
                                                href="{{route('cities.index')}}"><i class="icon-list"></i> كل المدن</a>
                                    </li>
                                    <li class="{{(Request::is('cities.create') ? 'active' : '')}}"><a
                                                href="{{route('cities.create')}}"><i class="icon-add-to-list"></i> اضافة
                                            مدينة</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>المناطق</span></a>
                                <ul>
                                    <li class="{{(Request::is('regions.index') ? 'active' : '')}}"><a
                                                href="{{route('regions.index')}}"><i class="icon-list"></i> كل المناطق</a>
                                    </li>
                                    <li class="{{(Request::is('regions.create') ? 'active' : '')}}"><a
                                                href="{{route('regions.create')}}"><i class="icon-add-to-list"></i> اضافة
                                            منطقة</a>
                                    </li>
                                </ul>
                            </li>


                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>العربات</span></a>
                                <ul>
                                    <li class="{{(Request::is('carts.index') ? 'active' : '')}}"><a
                                                href="{{route('carts.index')}}"><i class="icon-list"></i> كل العربات</a>
                                    </li>
                                </ul>
                            </li>


                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>اكواب المياه</span></a>
                                <ul>
                                    <li class="{{(Request::is('users_waters') ? 'active' : '')}}"><a
                                                href="{{route('users_waters')}}"><i class="icon-list"></i> اكواب المياة للمستخدمين</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="icon-users2"></i> <span>الرسائل</span></a>
                                <ul>
                                    <li class="{{(Request::is('chats.index') ? 'active' : '')}}"><a
                                                href="{{route('chats.index')}}"><i class="icon-list"></i> رسائل المستخدمين</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->
    <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><a href="{{Session::get('_previous')['url']}}" ><i class="icon-arrow-right6 position-left"></i></a>
                            <span class="text-semibold">لوحة تحكم دايتون </span> - @yield('title')</h4>
                    </div>

                    <div class="heading-elements">
                        <div class="heading-btn-group">
                            <a href="{{route('dashboard')}}"
                               class="btn btn-link btn-float has-text">
                                <i class="icon-bars-alt text-primary"></i><span>بعض الاحصائيات</span></a>
                            <a href="#" class="btn btn-link btn-float has-text"><i class="icon-users text-primary"></i> <span>أعضاء الاداره</span></a>
                        </div>

                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="{{asset('dashboard')}}"><i class="icon-home2 position-left"></i> لوحة التحكم</a></li>
                        <li class="active">@yield('title')</li>
                    </ul>
                </div>
            </div>
            <!-- /page header -->        <!-- Content area -->
            <div class="content">
            @include('admin.layouts.errors')
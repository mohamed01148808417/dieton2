@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group col-md-12 pull-left">
    <label>االرد علي رسالة المستخدم   </label>
    {!! Form::text("message",(isset($chat))?$chat->message : null,['class'=>'form-control ','placeholder'=>'اكتب الرساله للرد علي المستخدم  هنا'])!!}
    <input type="hidden" name="user_id" value="{{$user->id}}">
</div>

<br>
<br>
<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>

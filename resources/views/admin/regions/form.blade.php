@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group col-md-12 pull-left">
    <label>اسم الحي</label>
    {!! Form::text("name",(isset($region))?$region->name : null,['class'=>'form-control ','placeholder'=>'اكتب الاسم هنا'])!!}
</div>


<div class="form-group">
    <label>اختر المدينة </label>
    {{ Form::select('city_id', $cities,(isset($region))?$region->city_id : null , array('class'=>'form-control', 'placeholder'=>'اختار المدينة ')) }}
</div>




<br>
<br>
<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/terms', function () {
    return view('terms');
});
Route::get('/', function () {
    return view('welcome');
});
Route::get('admin/login','Admin\UsersController@login')->name('show_login');
Route::post('admin/login','Admin\UsersController@doLogin')->name('do_login');
Route::get('admin/logout','Admin\UsersController@logout')->name('do_logout');
//Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin','namespace'=>'Admin', 'middleware' => 'admin'], function()
{
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    // users route
    Route::resource('meals', 'MealsController');
    Route::resource('cities', 'CitiesController');
    Route::resource('regions', 'RegionsController');
    Route::resource('payNumbers', 'GenerateNumbersController');
    Route::resource('sports', 'SportsController');
    Route::resource('socials', 'SocialsController');
    Route::resource('chats', 'ChatsController');
    Route::get('chats/create/{user_id}', 'ChatsController@create');
    Route::resource('products', 'ProductsController');
    Route::resource('packages', 'PackagesController');
    Route::resource('carts', 'CartsController');
    Route::get('/orders/active/{id}','CartsController@active_order')->name('active_order');
    Route::get('/orders/refuse/{id}','CartsController@refuse_order')->name('refuse_order');
    Route::get('user/socials/{user_id}', 'UsersController@userSocials')->name('userSocials');
    Route::get('user/sports/{user_id}', 'SportsController@userSports')->name('userSports');
    Route::get('user/packages/{user_id}', 'PackagesController@userPackages')->name('packages');

    Route::get('package/active', 'PackagesController@activePackages')->name('active');

    Route::delete('package/destroy_package/{id}', 'PackagesController@destroy_package')->name('destroy_package');

    Route::post('package/active_package/{id}', 'PackagesController@active_package')->name('active_item');

    Route::post('package/un_active_package/{id}', 'PackagesController@un_active_package')->name('un_active_item');

    Route::get('package/un_active', 'PackagesController@unActivePackages')->name('un_active');
    // waters
    Route::get('users/waters', 'UsersController@waters')->name('users_waters');

    Route::get('user/carts/{user_id}', 'UsersController@userCarts')->name('userCarts');
    Route::resource('users', 'UsersController');
});
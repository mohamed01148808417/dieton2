<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// register for user
Route::get('/all', 'UserController@all');
Route::post('register', 'UserController@register');
//login user

Route::post('login', 'UserController@authenticate');
Route::get('', 'DataController@open');
Route::get('get/admin','UserController@getAdmin');

Route::group(['namespace' => 'Api'], function()
{


    // get all sports
    Route::get('sports','HomeController@getSports');
    Route::get('cities','HomeController@getCities');
    Route::get('regions/{city_id}','HomeController@getRegionByCityId');
    // get all meals
    Route::get('meals','HomeController@getMeals');
    // get all socials

    Route::get('socials/not_auth','HomeController@getSocialsNotAuth');

    Route::group(['middleware' => ['jwt.verify']], function() {
        Route::get('socials','HomeController@getSocials');
    });

    // get all packages
    Route::get('packages','HomeController@getPackages');
    // get all products with meal_id
    Route::get('products','HomeController@getAllProducts');

    Route::get('products/{meal_id}','HomeController@getProductsWithMeal');


});
Route::group(['middleware' => ['jwt.verify']], function() {
    Route::group(['prefix' => 'users'], function () {
        // get login user data
        Route::get('/', 'UserController@getAuthenticatedUser');


        Route::get('/all', 'UserController@all');

        // get user profile
        Route::get('profile/{id}', 'UserController@getUserProfile');

        Route::post('add_package', 'UserController@addPackage');
        // update user profile
        Route::post('update', 'UserController@update');
        //update password
        Route::post('update/password', 'UserController@updatePassword');
        // add played sport to profile
        Route::post('add/sport', 'UserController@addSport');
        // get today played sport
        Route::get('get/playedToday/sports/', 'UserController@getTodayPlayedSportsForUser');
        Route::get('get/played/sports/', 'UserController@getPlayedSportsForUser');
        Route::get('get/single_sport/{id}', 'UserController@getSingleSport');

        // get all socials for specific user
        Route::get('socials/{user_id}','UserController@getSocialsForUser');
        Route::get('socials/remove/{id}','UserController@remove_social');

        Route::get('/user/packages','UserController@getPackagesForLoginUser');
        // get all socials for login user
        Route::get('my_socials','UserController@getSocialsForLoginUser');
        Route::post('add_socials','UserController@addSocials');
        // sports put data
        //Route::put('sport/add','HomeController@addSport');
        // cart apis
        Route::get('my-cart/{order_type}','UserController@my_cart');
        Route::get('my-cart/added','UserController@getList');
        Route::post('add-cart','UserController@add_cart');
        Route::get('cart_numbers','UserController@countNumbersInCart');

        Route::post('process/cart','UserController@processCarts');

        Route::delete('remove-cart/{cart_id}','UserController@remove_cart');
        Route::post('update-cart/{cart_id}','UserController@update_cart');



        // regions and cities


        // chats APIS

        Route::get('my-chat-with-users','UserController@my_chats');
        Route::get('my-chat-with-user/{user_id}','UserController@chatWithUser');
        Route::post('send/message/specific-user','UserController@sendMessageToUser');



        // waters
        Route::get('waters/today','UserController@getTodayCupOfWaterForUser');
        Route::post('waters/add/cup_of_water','UserController@addCupOfWater');


        // likes
        Route::get('user/like/social/{social_id}/{type}','UserController@likeOrLoveSocial');

        // reports
        // get today report
        Route::get('user/today_report','UserController@getTodayReport');
        Route::post('user/report/date','UserController@getDayReport');
        Route::get('user/dates/{user_id}','UserController@getDaysThatUserPlaySports');

    });




});
